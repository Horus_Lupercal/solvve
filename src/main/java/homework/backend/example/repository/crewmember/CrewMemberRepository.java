package homework.backend.example.repository.crewmember;

import homework.backend.example.domain.persons.CrewMember;
import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

public interface CrewMemberRepository extends CrudRepository<CrewMember, UUID> {
}
