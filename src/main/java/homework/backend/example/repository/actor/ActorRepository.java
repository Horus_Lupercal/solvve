package homework.backend.example.repository.actor;

import homework.backend.example.domain.persons.Actor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ActorRepository extends CrudRepository<Actor, UUID> {
}
