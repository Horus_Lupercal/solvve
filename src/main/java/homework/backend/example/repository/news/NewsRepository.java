package homework.backend.example.repository.news;

import homework.backend.example.domain.content.News;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface NewsRepository extends CrudRepository<News, UUID> {
}
