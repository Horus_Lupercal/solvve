package homework.backend.example.repository.reviewcheck;

import homework.backend.example.domain.ententies.ReviewCheck;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ReviewCheckRepository extends CrudRepository<ReviewCheck, UUID> {
}
