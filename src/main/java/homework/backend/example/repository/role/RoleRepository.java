package homework.backend.example.repository.role;

import homework.backend.example.domain.content.Role;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

@Repository
public interface RoleRepository extends CrudRepository<Role, UUID>, RoleRepositoryCustom {
    List<Role> findByActorIdAndPay(UUID actorId, Long pay);

    @Query("select r from Role r where r.actor.id = :actorId and r.pay > :pay")
    List<Role> findByActorIdByPayGreaterThan(UUID actorId, Long pay);

    @Query("select r.id from Role r")
    Stream<UUID> getIdsOfRoles();
}
