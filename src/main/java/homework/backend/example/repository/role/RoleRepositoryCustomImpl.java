package homework.backend.example.repository.role;

import homework.backend.example.domain.content.Role;
import homework.backend.example.dto.role.RoleFilter;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class RoleRepositoryCustomImpl implements RoleRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Role> findByFilter(RoleFilter filter) {
        StringBuilder sb = new StringBuilder();
        sb.append("select r from Role r where 1=1");
        if (filter.getActorId() != null) {
            sb.append(" and r.actor.id = :actorId");
        }
        if (filter.getFilmId() != null) {
            sb.append(" and r.film.id = :filmId");
        }
        if (filter.getPay() != null) {
            sb.append(" and r.pay > :pay");
        }
        if (filter.getAverageMark() != null) {
            sb.append(" and r.averageMark > :averageMark");
        }

        TypedQuery<Role> query = entityManager.createQuery(sb.toString(), Role.class);

        if (filter.getActorId() != null) {
            query.setParameter("actorId", filter.getActorId());
        }
        if (filter.getFilmId() != null) {
            query.setParameter("filmId", filter.getFilmId());
        }
        if (filter.getPay() != null) {
            query.setParameter("pay", filter.getPay());
        }
        if (filter.getAverageMark() != null) {
            query.setParameter("averageMark", filter.getAverageMark());
        }
        return query.getResultList();
    }
}
