package homework.backend.example.repository.role;

import homework.backend.example.domain.content.Role;
import homework.backend.example.dto.role.RoleFilter;

import java.util.List;

public interface RoleRepositoryCustom {
    List<Role> findByFilter(RoleFilter filter);
}

