package homework.backend.example.repository.registereduser;

import homework.backend.example.domain.users.RegisteredUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RegisteredUserRepository extends CrudRepository<RegisteredUser, UUID> {
}
