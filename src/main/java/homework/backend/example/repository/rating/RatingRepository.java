package homework.backend.example.repository.rating;

import homework.backend.example.domain.ententies.rating.Rating;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RatingRepository extends CrudRepository<Rating, UUID> {

    @Query("select avg(r.rating) from Rating r where r.film.id = :filmId")
    Double calcAverageMarkOfFilm(UUID filmId);

    @Query("select avg(r.rating) from Rating r where r.role.id = :roleId")
    Double calcAverageMarkOfRole(UUID roleId);
}
