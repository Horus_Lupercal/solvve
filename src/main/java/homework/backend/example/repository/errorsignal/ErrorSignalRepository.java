package homework.backend.example.repository.errorsignal;

import homework.backend.example.domain.enums.VerificationStatusType;
import homework.backend.example.domain.massages.errormassages.ErrorSignal;
import homework.backend.example.domain.massages.errormassages.misprintsignal.FilmMisprintSignal;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ErrorSignalRepository extends CrudRepository<ErrorSignal, UUID> {

    List<FilmMisprintSignal> findByVerification(VerificationStatusType verification);

    List<FilmMisprintSignal> findByWrongString(String wrongString);

    @Query("select f from FilmMisprintSignal f where f.verification = :verification and f.wrongString = :wrongString")
    List<FilmMisprintSignal> findByVerificationAndWrongString(VerificationStatusType verification, String wrongString);

    @Query("select f from FilmMisprintSignal f where f.film.id = :filmId and f.wrongString = :wrongString")
    List<FilmMisprintSignal> findByFilmIdAndWrongString(UUID filmId, String wrongString);
}
