package homework.backend.example.dto.film;

import lombok.Data;

import java.time.LocalDate;

@Data
public class FilmPutDTO {
    private String title;
    private String description;
    private Long budget;
    private LocalDate release;
    private Long fees;
    private Boolean status;
    private Double averageMark;
}
