package homework.backend.example.dto.film;

import lombok.Data;

import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

@Data
public class FilmReadDTO {
    private UUID id;
    private String title;
    private String description;
    private Long budget;
    private LocalDate release;
    private Long fees;
    private Boolean status;
    private Double averageMark;
    private Instant createdAt;
    private Instant updatedAt;
}
