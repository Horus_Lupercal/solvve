package homework.backend.example.dto.registreduser;

import homework.backend.example.domain.enums.SexType;
import homework.backend.example.domain.enums.UserAccessType;
import homework.backend.example.domain.enums.UserPostType;
import homework.backend.example.domain.enums.UserTrustLevelType;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class RegisteredUserReadDTO {
    private UUID id;
    private String name;
    private String email;
    private String password;
    private SexType sex;
    private UserTrustLevelType trustLevel;
    private UserAccessType accessType;
    private UserPostType postType;
    private Instant createdAt;
    private Instant updatedAt;
}
