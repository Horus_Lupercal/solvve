package homework.backend.example.dto.actor;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class ActorReadDTO {
    private UUID id;
    private Long middlePay;
    private UUID personId;
    private Instant createdAt;
    private Instant updatedAt;
}
