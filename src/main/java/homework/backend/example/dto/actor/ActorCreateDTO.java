package homework.backend.example.dto.actor;

import lombok.Data;

import java.util.UUID;

@Data
public class ActorCreateDTO {
    private Long middlePay;
    private UUID personId;
}
