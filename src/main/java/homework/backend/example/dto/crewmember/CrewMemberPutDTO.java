package homework.backend.example.dto.crewmember;

import homework.backend.example.domain.enums.CrewMemberPostType;
import lombok.Data;

@Data
public class CrewMemberPutDTO {
    private CrewMemberPostType post;
}
