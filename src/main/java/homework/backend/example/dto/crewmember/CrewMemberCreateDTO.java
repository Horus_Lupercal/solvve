package homework.backend.example.dto.crewmember;

import homework.backend.example.domain.enums.CrewMemberPostType;
import lombok.Data;

import java.util.UUID;

@Data
public class CrewMemberCreateDTO {
    private UUID personId;
    private UUID filmId;
    private CrewMemberPostType post;
}
