package homework.backend.example.dto.crewmember;

import homework.backend.example.domain.enums.CrewMemberPostType;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class CrewMemberReadDTO {
    private UUID id;
    private UUID personId;
    private UUID filmId;
    private CrewMemberPostType post;
    private Instant createdAt;
    private Instant updatedAt;
}
