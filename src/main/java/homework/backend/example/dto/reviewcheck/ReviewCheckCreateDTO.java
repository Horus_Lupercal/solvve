package homework.backend.example.dto.reviewcheck;

import homework.backend.example.domain.enums.ReviewCheckStatusType;
import lombok.Data;

import java.util.UUID;

@Data
public class ReviewCheckCreateDTO {
    private UUID moderatorId;
    private ReviewCheckStatusType checkStatus;
}
