package homework.backend.example.dto.reviewcheck;

import homework.backend.example.domain.enums.ReviewCheckStatusType;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class ReviewCheckReadDTO {
    private UUID id;
    private UUID moderatorId;
    private ReviewCheckStatusType checkStatus;
    private Instant createdAt;
    private Instant updatedAt;
}
