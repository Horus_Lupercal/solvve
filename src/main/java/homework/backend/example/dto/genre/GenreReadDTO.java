package homework.backend.example.dto.genre;

import homework.backend.example.domain.enums.GenreType;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class GenreReadDTO {
    private UUID id;
    private String description;
    private GenreType genreType;
    private Instant createdAt;
    private Instant updatedAt;
}
