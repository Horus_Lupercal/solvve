package homework.backend.example.dto.genre;

import homework.backend.example.domain.enums.GenreType;
import lombok.Data;

@Data
public class GenrePutDTO {
    private String description;
    private GenreType genreType;
}
