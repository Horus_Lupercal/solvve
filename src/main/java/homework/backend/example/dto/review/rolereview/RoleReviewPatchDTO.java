package homework.backend.example.dto.review.rolereview;

import homework.backend.example.domain.enums.VerificationStatusType;
import lombok.Data;

import java.util.UUID;

@Data
public class RoleReviewPatchDTO {
    private String review;
    private VerificationStatusType verification;
    private UUID reviewCheckId;
    private UUID roleId;
}
