package homework.backend.example.dto.review.filmreview;

import homework.backend.example.domain.enums.VerificationStatusType;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class FilmReviewReadDTO {
    private UUID id;
    private String review;
    private UUID userId;
    private VerificationStatusType verification;
    private UUID reviewCheckId;
    private UUID filmId;
    private Instant createdAt;
    private Instant updatedAt;
}
