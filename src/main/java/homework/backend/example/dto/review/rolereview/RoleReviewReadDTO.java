package homework.backend.example.dto.review.rolereview;

import homework.backend.example.domain.enums.VerificationStatusType;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class RoleReviewReadDTO {
    private UUID id;
    private String review;
    private UUID userId;
    private VerificationStatusType verification;
    private UUID reviewCheckId;
    private UUID roleId;
    private Instant createdAt;
    private Instant updatedAt;
}
