package homework.backend.example.dto.review.filmreview;

import homework.backend.example.domain.enums.VerificationStatusType;
import lombok.Data;

import java.util.UUID;

@Data
public class FilmReviewCreateDTO {
    private String review;
    private UUID userId;
    private VerificationStatusType verification;
    private UUID reviewCheckId;
    private UUID filmId;
}
