package homework.backend.example.dto.news;

import lombok.Data;

@Data
public class NewsCreateDTO {
    private String text;
}
