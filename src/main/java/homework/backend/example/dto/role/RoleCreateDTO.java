package homework.backend.example.dto.role;

import lombok.Data;

import java.util.UUID;

@Data
public class RoleCreateDTO {
    private UUID actorId;
    private UUID filmId;
    private String description;
    private Long pay;
    private Double averageMark;
}
