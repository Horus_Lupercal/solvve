package homework.backend.example.dto.role;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class RoleReadDTO {
    private UUID id;
    private UUID actorId;
    private UUID filmId;
    private String description;
    private Long pay;
    private Double averageMark;
    private Instant createdAt;
    private Instant updatedAt;
}
