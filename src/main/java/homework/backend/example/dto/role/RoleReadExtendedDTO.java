package homework.backend.example.dto.role;

import homework.backend.example.dto.actor.ActorReadDTO;
import homework.backend.example.dto.film.FilmReadDTO;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class RoleReadExtendedDTO {
    private UUID id;
    private ActorReadDTO actor;
    private FilmReadDTO film;
    private String description;
    private Long pay;
    private Double averageMark;
    private Instant createdAt;
    private Instant updatedAt;
}
