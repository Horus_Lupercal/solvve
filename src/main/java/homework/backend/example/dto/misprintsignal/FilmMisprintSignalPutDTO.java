package homework.backend.example.dto.misprintsignal;

import homework.backend.example.domain.enums.VerificationStatusType;
import lombok.Data;

import java.util.UUID;

@Data
public class FilmMisprintSignalPutDTO {
    private String wrongString;
    private String correctString;
    private VerificationStatusType verification;
    private UUID filmId;
}
