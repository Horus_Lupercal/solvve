package homework.backend.example.dto.misprintsignal;

import homework.backend.example.domain.enums.VerificationStatusType;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class FilmMisprintSignalReadDTO {
    private UUID id;
    private String wrongString;
    private String correctString;
    private Instant fixDate;
    private VerificationStatusType verification;
    private UUID userId;
    private UUID filmId;
    private UUID contentManagerId;
    private Instant createdAt;
    private Instant updatedAt;
}
