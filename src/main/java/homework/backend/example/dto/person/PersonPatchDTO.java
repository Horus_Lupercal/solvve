package homework.backend.example.dto.person;

import homework.backend.example.domain.enums.SexType;
import lombok.Data;

import java.time.LocalDate;

@Data
public class PersonPatchDTO {
    private String name;
    private String biography;
    private LocalDate birthday;
    private SexType sex;
}
