package homework.backend.example.dto.person;

import homework.backend.example.domain.enums.SexType;
import lombok.Data;

import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

@Data
public class PersonReadDTO {
    private UUID id;
    private String name;
    private String biography;
    private LocalDate birthday;
    private SexType sex;
    private Instant createdAt;
    private Instant updatedAt;
}
