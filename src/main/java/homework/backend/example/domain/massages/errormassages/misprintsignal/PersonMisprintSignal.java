package homework.backend.example.domain.massages.errormassages.misprintsignal;

import homework.backend.example.domain.massages.errormassages.ErrorSignal;
import homework.backend.example.domain.persons.Person;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity
@DiscriminatorValue("person")
public class PersonMisprintSignal extends ErrorSignal {
    @ManyToOne
    @JoinColumn(name = "person_id")
    private Person person;
}
