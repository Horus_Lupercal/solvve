package homework.backend.example.domain.massages.comment;

import homework.backend.example.domain.AbstractEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity
public class Comment extends AbstractEntity {
    private String text;

    //@OneToMany
    //private Set<Like> likes = new HashSet<>();
}
