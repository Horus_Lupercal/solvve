package homework.backend.example.domain.massages.errormassages.misprintsignal;

import homework.backend.example.domain.content.Genre;
import homework.backend.example.domain.massages.errormassages.ErrorSignal;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity
@DiscriminatorValue("genre")
public class GenreMisprintSignal extends ErrorSignal {
    @ManyToOne
    @JoinColumn(name = "genre_id")
    private Genre genre;
}
