package homework.backend.example.domain.massages.errormassages.misprintsignal;

import homework.backend.example.domain.content.Film;
import homework.backend.example.domain.massages.errormassages.ErrorSignal;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@DiscriminatorValue("film")
public class FilmMisprintSignal extends ErrorSignal {
    @ManyToOne
    @JoinColumn(name = "film_id")
    private Film film;
}
