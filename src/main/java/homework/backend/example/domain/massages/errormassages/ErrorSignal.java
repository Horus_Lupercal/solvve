package homework.backend.example.domain.massages.errormassages;

import homework.backend.example.domain.AbstractEntity;
import homework.backend.example.domain.enums.VerificationStatusType;
import homework.backend.example.domain.users.RegisteredUser;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.Instant;

@Getter
@Setter
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "error_signal_type",
        discriminatorType = DiscriminatorType.STRING)
@EntityListeners(AuditingEntityListener.class)
public abstract class ErrorSignal extends AbstractEntity {
    private String wrongString;
    private String correctString;
    private Instant fixDate;

    @ManyToOne
    @JoinColumn(name = "content_manager_id")
    private RegisteredUser contentManager;

    @Enumerated(EnumType.STRING)
    private VerificationStatusType verification;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private RegisteredUser user;
}
