package homework.backend.example.domain.massages.errormassages.errorsignal;

import homework.backend.example.domain.enums.ReasonType;
import homework.backend.example.domain.massages.review.Review;
import homework.backend.example.domain.massages.errormassages.ErrorSignal;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@DiscriminatorValue("review")
public class ReviewErrorSignal extends ErrorSignal {
    @Enumerated(EnumType.STRING)
    private ReasonType reasonType;

    @ManyToOne
    @JoinColumn(name = "review_id")
    private Review review;
}
