package homework.backend.example.domain.massages.errormassages.errorsignal;

import homework.backend.example.domain.enums.ReasonType;
import homework.backend.example.domain.massages.comment.Comment;
import homework.backend.example.domain.massages.errormassages.ErrorSignal;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@DiscriminatorValue("comment")
public class CommentErrorSignal extends ErrorSignal {
    @Enumerated(EnumType.STRING)
    private ReasonType reasonType;

    @ManyToOne
    @JoinColumn(name = "comment_id")
    private Comment comment;
}
