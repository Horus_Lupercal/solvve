package homework.backend.example.domain.massages.review;

import homework.backend.example.domain.AbstractEntity;
import homework.backend.example.domain.ententies.ReviewCheck;
import homework.backend.example.domain.enums.VerificationStatusType;
import homework.backend.example.domain.users.RegisteredUser;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "review_type",
        discriminatorType = DiscriminatorType.STRING)
public abstract class Review extends AbstractEntity {
    private String review;
    //private Set<Like> likes = new HashSet<>();

    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private RegisteredUser user;

    @Enumerated(EnumType.STRING)
    private VerificationStatusType verification;

    @OneToOne
    @JoinColumn(name = "review_check_id")
    private ReviewCheck reviewCheck;
}
