package homework.backend.example.domain.massages.errormassages.misprintsignal;

import homework.backend.example.domain.content.News;
import homework.backend.example.domain.massages.errormassages.ErrorSignal;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity
@DiscriminatorValue("news")
public class NewsMisprintSignal extends ErrorSignal {
    @ManyToOne
    @JoinColumn(name = "news_id")
    private News news;
}
