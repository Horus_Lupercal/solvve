package homework.backend.example.domain.massages.review;

import homework.backend.example.domain.content.Film;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity
@DiscriminatorValue("film")
public class FilmReview extends Review {
    @ManyToOne
    @JoinColumn(name = "film_id")
    private Film film;
}
