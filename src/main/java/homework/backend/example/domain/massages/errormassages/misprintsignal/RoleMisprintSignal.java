package homework.backend.example.domain.massages.errormassages.misprintsignal;

import homework.backend.example.domain.content.Role;
import homework.backend.example.domain.massages.errormassages.ErrorSignal;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity
@DiscriminatorValue("role")
public class RoleMisprintSignal extends ErrorSignal {
    @ManyToOne
    @JoinColumn(name = "Role_id")
    private Role role;
}
