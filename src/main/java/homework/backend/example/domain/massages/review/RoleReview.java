package homework.backend.example.domain.massages.review;

import homework.backend.example.domain.content.Role;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity
@DiscriminatorValue("role")
public class RoleReview extends Review {
    @ManyToOne
    @JoinColumn(name = "role_id")
    private Role role;
}
