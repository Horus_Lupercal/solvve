package homework.backend.example.domain.ententies.rating.rolerating;

import homework.backend.example.domain.content.Role;
import homework.backend.example.domain.ententies.rating.Rating;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity
@DiscriminatorValue("role")
public class RoleRating extends Rating {

    @ManyToOne
    private Role role;
}
