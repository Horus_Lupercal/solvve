package homework.backend.example.domain.ententies;

import homework.backend.example.domain.AbstractEntity;
import homework.backend.example.domain.users.RegisteredUser;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity
public class Like extends AbstractEntity {
    @ManyToOne
    @JoinColumn(nullable = false, updatable = false)
    private RegisteredUser user;

    private Boolean like;

    //@ManyToOne
    //private Comment comment;
}
