package homework.backend.example.domain.ententies.rating;

import homework.backend.example.domain.AbstractEntity;
import homework.backend.example.domain.users.RegisteredUser;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "rating_type",
        discriminatorType = DiscriminatorType.STRING)
public abstract class Rating extends AbstractEntity {
    private Double rating;

    @ManyToOne
    private RegisteredUser user;
}
