package homework.backend.example.domain.ententies.rating.filmrating;

import homework.backend.example.domain.content.Film;
import homework.backend.example.domain.ententies.rating.Rating;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity
@DiscriminatorValue("film")
public class FilmRating extends Rating {

    @ManyToOne
    private Film film;
}
