package homework.backend.example.domain.users;

import homework.backend.example.domain.AbstractEntity;
import homework.backend.example.domain.ententies.Like;
import homework.backend.example.domain.ententies.ReviewCheck;
import homework.backend.example.domain.enums.SexType;
import homework.backend.example.domain.enums.UserAccessType;
import homework.backend.example.domain.enums.UserPostType;
import homework.backend.example.domain.enums.UserTrustLevelType;
import homework.backend.example.domain.massages.review.Review;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity
public class RegisteredUser extends AbstractEntity {
    private String name;
    private String email;
    private String password;

    @Enumerated(EnumType.STRING)
    private SexType sex;

    @Enumerated(EnumType.STRING)
    private UserTrustLevelType trustLevel;

    @Enumerated(EnumType.STRING)
    private UserAccessType accessType;

    @Enumerated(EnumType.STRING)
    private UserPostType postType;

    @OneToMany(mappedBy = "user")
    private Set<Like> likes;

    @OneToMany(mappedBy = "user")
    private Set<Review> reviews;

    @OneToMany(mappedBy = "moderator")
    private Set<ReviewCheck> checks;
}
