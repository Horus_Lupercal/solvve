package homework.backend.example.domain.persons;

import homework.backend.example.domain.AbstractEntity;
import homework.backend.example.domain.content.Film;
import homework.backend.example.domain.enums.CrewMemberPostType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity
public class CrewMember extends AbstractEntity {
    @ManyToOne
    private Person person;

    @ManyToOne
    private Film film;

    @Enumerated(EnumType.STRING)
    private CrewMemberPostType post;
}
