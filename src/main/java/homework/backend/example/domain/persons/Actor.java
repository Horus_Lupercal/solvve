package homework.backend.example.domain.persons;

import homework.backend.example.domain.AbstractEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity
public class Actor extends AbstractEntity {
    private Long middlePay;

    @OneToOne
    @JoinColumn(name = "person_id")
    private Person person;
}
