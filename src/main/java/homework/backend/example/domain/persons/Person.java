package homework.backend.example.domain.persons;

import homework.backend.example.domain.AbstractEntity;
import homework.backend.example.domain.enums.SexType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDate;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity
public class Person extends AbstractEntity {
    private String name;
    private String biography;
    private LocalDate birthday;

    @Enumerated(EnumType.STRING)
    private SexType sex;

}
