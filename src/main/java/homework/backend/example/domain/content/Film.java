package homework.backend.example.domain.content;

import homework.backend.example.domain.AbstractEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import java.time.LocalDate;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity
public class Film extends AbstractEntity {
    private String title;
    private String description;
    private Long budget;

    private LocalDate release;
    private Long fees;
    private Boolean status;
    private Double averageMark;
}
