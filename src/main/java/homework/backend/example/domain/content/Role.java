package homework.backend.example.domain.content;

import homework.backend.example.domain.AbstractEntity;
import homework.backend.example.domain.persons.Actor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity
public class Role extends AbstractEntity {
    @ManyToOne
    private Actor actor;

    @ManyToOne
    private Film film;

    private String description;
    private Long pay;
    private Double averageMark;
}
