package homework.backend.example.domain.content;

import homework.backend.example.domain.AbstractEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity
public class News extends AbstractEntity {
    private String text;
}
