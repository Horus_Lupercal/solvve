package homework.backend.example.domain.content;

import homework.backend.example.domain.AbstractEntity;
import homework.backend.example.domain.enums.GenreType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@Entity
public class Genre extends AbstractEntity {
    private String description;

    @Enumerated(EnumType.STRING)
    private GenreType genreType;

    //@ManyToMany
    //private Set<Film> films;
}
