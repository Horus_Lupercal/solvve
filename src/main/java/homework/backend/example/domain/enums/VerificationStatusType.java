package homework.backend.example.domain.enums;

public enum VerificationStatusType {
    CHECKED,
    NOT_CHECKED
}
