package homework.backend.example.domain.enums;

public enum GenreType {
    ACTION,
    ADVENTURE,
    ANIMATION,
    BIOGRAPHY,
    COMEDY,
    CRIME,
    DOCUMENTARY,
    DRAMA,
    FAMILY,
    FANTASY,
    HISTORY,
    HORROR,
    MUSIC,
    MUSICAL,
    MYSTERY,
    ROMANCE,
    SCI_FI,
    SHORT_FILM,
    SPORT,
    SUPERHERO,
    THRILLER,
    WAR,
    WESTERN
}
