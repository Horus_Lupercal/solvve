package homework.backend.example.domain.enums;

public enum UserPostType {
    REGISTERED_USER,
    ADMIN,
    MODERATOR,
    CONTENT_MANAGER
}
