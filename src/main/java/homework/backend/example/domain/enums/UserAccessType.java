package homework.backend.example.domain.enums;

public enum UserAccessType {
    BAN,
    ALLOW,
    ONLY_READ,
    DELETED
}
