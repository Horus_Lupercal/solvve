package homework.backend.example.domain.enums;

public enum ReviewCheckStatusType {
    OK,
    RETURN,
    EDITS
}
