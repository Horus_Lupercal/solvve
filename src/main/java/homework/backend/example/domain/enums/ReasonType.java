package homework.backend.example.domain.enums;

public enum ReasonType {
    CENSURE,
    INSULT,
    SPOILER,
    SPAM
}
