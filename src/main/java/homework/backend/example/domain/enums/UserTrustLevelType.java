package homework.backend.example.domain.enums;

public enum UserTrustLevelType {
    HIGH,
    MEDIUM,
    LOW
}
