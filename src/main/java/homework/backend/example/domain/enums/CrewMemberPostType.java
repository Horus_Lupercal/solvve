package homework.backend.example.domain.enums;

public enum CrewMemberPostType {
    DIRECTOR,
    PRODUCER,
    CAMERAMAN,
    SCREEN_WRITER
}
