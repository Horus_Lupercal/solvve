package homework.backend.example.domain.enums;

public enum SexType {
    MALE,
    FEMALE
}
