package homework.backend.example.job;

import homework.backend.example.repository.role.RoleRepository;
import homework.backend.example.service.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Component
public class UpdateAverageMarkOfRolesJob {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RoleService roleService;

    @Transactional(readOnly = true)
    @Scheduled(cron = "${update.average.mark.of.roles.job.cron}")
    public void updateAverageMarkOfRoles() {
        log.info("Job started");

        roleRepository.getIdsOfRoles().forEach(roleId -> {
            try {
                roleService.updateAverageMarkOfRole(roleId);
            } catch (Exception e) {
                log.error("Failed to update average mark for role: {}", roleId, e);
            }
        });
        log.info("Job finished");
    }
}
