package homework.backend.example.controller;

import homework.backend.example.dto.crewmember.CrewMemberCreateDTO;
import homework.backend.example.dto.crewmember.CrewMemberPatchDTO;
import homework.backend.example.dto.crewmember.CrewMemberPutDTO;
import homework.backend.example.dto.crewmember.CrewMemberReadDTO;
import homework.backend.example.service.CrewMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/crew")
public class CrewMemberController {

    @Autowired
    private CrewMemberService crewMemberService;

    @GetMapping("/{id}")
    public CrewMemberReadDTO getCrewMember(@PathVariable UUID id) {
        return crewMemberService.getCrewMember(id);
    }

    @PostMapping
    public CrewMemberReadDTO createCrewMember(@RequestBody CrewMemberCreateDTO create) {
        return crewMemberService.createCrewMember(create);
    }

    @PatchMapping("/{id}")
    public CrewMemberReadDTO patchCrewMember(@PathVariable UUID id, @RequestBody CrewMemberPatchDTO patch) {
        return crewMemberService.patchCrewMember(id, patch);
    }

    @PutMapping("/{id}")
    public CrewMemberReadDTO updateCrewMember(@PathVariable UUID id, @RequestBody CrewMemberPutDTO put) {
        return crewMemberService.updateCrewMember(id, put);
    }

    @DeleteMapping("/{id}")
    public void deleteCrewMember(@PathVariable UUID id) {
        crewMemberService.deleteCrewMember(id);
    }
}
