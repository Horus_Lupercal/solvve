package homework.backend.example.controller;

import homework.backend.example.dto.registreduser.RegisteredUserCreateDTO;
import homework.backend.example.dto.registreduser.RegisteredUserPatchDTO;
import homework.backend.example.dto.registreduser.RegisteredUserPutDTO;
import homework.backend.example.dto.registreduser.RegisteredUserReadDTO;
import homework.backend.example.service.RegisteredUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/users")
public class RegisteredUserController {
    @Autowired
    private RegisteredUserService userService;

    @GetMapping("/{id}")
    public RegisteredUserReadDTO getUser(@PathVariable UUID id) {
        return userService.getUser(id);
    }

    @PostMapping()
    public RegisteredUserReadDTO createUser(@RequestBody RegisteredUserCreateDTO create) {
        return userService.createUser(create);
    }

    @PatchMapping("/{id}")
    public RegisteredUserReadDTO patchUser(@PathVariable UUID id, @RequestBody RegisteredUserPatchDTO patch) {
        return userService.patchUser(id, patch);
    }

    @PutMapping("/{id}")
    public RegisteredUserReadDTO putUser(@PathVariable UUID id, @RequestBody RegisteredUserPutDTO put) {
        return userService.upDateUser(id, put);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable UUID id) {
        userService.deleteUser(id);
    }
}
