package homework.backend.example.controller;

import homework.backend.example.dto.actor.ActorCreateDTO;
import homework.backend.example.dto.actor.ActorPatchDTO;
import homework.backend.example.dto.actor.ActorPutDTO;
import homework.backend.example.dto.actor.ActorReadDTO;
import homework.backend.example.service.ActorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/actors")
public class ActorController {

    @Autowired
    private ActorService actorService;

    @GetMapping("/{id}")
    public ActorReadDTO getActor(@PathVariable UUID id) {
        return actorService.getActor(id);
    }

    @PostMapping
    public ActorReadDTO createActor(@RequestBody ActorCreateDTO create) {
        return actorService.createActor(create);
    }

    @PatchMapping("/{id}")
    public ActorReadDTO patchActor(@PathVariable UUID id, @RequestBody ActorPatchDTO patch) {
        return actorService.patchActor(id, patch);
    }

    @PutMapping("/{id}")
    public ActorReadDTO putActor(@PathVariable UUID id, @RequestBody ActorPutDTO put) {
        return actorService.upDateActor(id, put);
    }

    @DeleteMapping("/{id}")
    public void deleteActor(@PathVariable UUID id) {
        actorService.deleteActor(id);
    }
}
