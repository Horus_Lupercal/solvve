package homework.backend.example.controller;

import homework.backend.example.dto.person.PersonCreateDTO;
import homework.backend.example.dto.person.PersonPatchDTO;
import homework.backend.example.dto.person.PersonPutDTO;
import homework.backend.example.dto.person.PersonReadDTO;
import homework.backend.example.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/persons")
public class PersonController {

    @Autowired
    private PersonService personService;

    @GetMapping("/{id}")
    public PersonReadDTO getPerson(@PathVariable UUID id) {
        return personService.getPerson(id);
    }

    @PostMapping()
    public PersonReadDTO createPerson(@RequestBody PersonCreateDTO createDTO) {
        return personService.createPerson(createDTO);
    }

    @PatchMapping("/{id}")
    public PersonReadDTO patchPerson(@PathVariable UUID id, @RequestBody PersonPatchDTO patchDTO) {
        return personService.patchPerson(id, patchDTO);
    }

    @PutMapping("/{id}")
    public PersonReadDTO putPerson(@PathVariable UUID id, @RequestBody PersonPutDTO putDTO) {
        return personService.upDatePerson(id, putDTO);
    }

    @DeleteMapping("/{id}")
    public void deletePerson(@PathVariable UUID id) {
        personService.deletePerson(id);
    }
}
