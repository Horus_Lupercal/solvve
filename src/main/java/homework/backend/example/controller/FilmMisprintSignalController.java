package homework.backend.example.controller;

import homework.backend.example.dto.misprintsignal.FilmMisprintSignalCreateDTO;
import homework.backend.example.dto.misprintsignal.FilmMisprintSignalPatchDTO;
import homework.backend.example.dto.misprintsignal.FilmMisprintSignalPutDTO;
import homework.backend.example.dto.misprintsignal.FilmMisprintSignalReadDTO;
import homework.backend.example.service.FilmMisprintSignalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/v1/api/filmMisprintSignals")
public class FilmMisprintSignalController {

    @Autowired
    private FilmMisprintSignalService filmMisprintSignalService;

    @GetMapping("/{id}")
    public FilmMisprintSignalReadDTO getFilmMisprintSignal(@PathVariable UUID id) {
        return filmMisprintSignalService.getFilmMisprintSignal(id);
    }

    @PostMapping
    public FilmMisprintSignalReadDTO createFilmMisprintSignal(@RequestBody FilmMisprintSignalCreateDTO createDTO) {
        return filmMisprintSignalService.createFilmMisprintSignal(createDTO);
    }

    @PatchMapping("/{id}")
    public FilmMisprintSignalReadDTO patchFilmMisprintSignal(
            @PathVariable UUID id, @RequestBody FilmMisprintSignalPatchDTO patch) {
        return filmMisprintSignalService.patchFilmMisprintSignal(id, patch);
    }

    @DeleteMapping("/{id}")
    public void deleteFilmMisprintSignal(@PathVariable UUID id) {
        filmMisprintSignalService.deleteFilmMisprintSignal(id);
    }

    @PutMapping("/{id}")
    public FilmMisprintSignalReadDTO putFilmMisprintSignal(
            @PathVariable UUID id, @RequestBody FilmMisprintSignalPutDTO put) {
        return filmMisprintSignalService.updateFilmMisprintSignal(id, put);
    }
}
