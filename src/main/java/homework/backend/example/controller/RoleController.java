package homework.backend.example.controller;

import homework.backend.example.dto.role.*;
import homework.backend.example.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/roles")
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping("/extended/{id}")
    public RoleReadExtendedDTO getRoleExt(@PathVariable UUID id) {
        return roleService.getRoleExt(id);
    }

    @GetMapping("/{id}")
    public RoleReadDTO getRole(@PathVariable UUID id) {
        return roleService.getRole(id);
    }

    @GetMapping
    public List<RoleReadDTO> getRoles(RoleFilter filter) {
        return roleService.getRoles(filter);
    }

    @PostMapping
    public RoleReadDTO createRole(@RequestBody RoleCreateDTO createDTO) {
        return roleService.createRole(createDTO);
    }

    @DeleteMapping("/{id}")
    public void deleteRole(@PathVariable UUID id) {
        roleService.deleteRole(id);
    }

    @PatchMapping("/{id}")
    public RoleReadDTO patchRole(@PathVariable UUID id, @RequestBody RolePatchDTO patch) {
        return roleService.patchRole(id, patch);
    }

    @PutMapping("/{id}")
    public RoleReadDTO putRole(@PathVariable UUID id, @RequestBody RolePutDTO put) {
        return roleService.upDateRole(id, put);
    }
}
