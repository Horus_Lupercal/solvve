package homework.backend.example.controller;

import homework.backend.example.dto.review.rolereview.RoleReviewCreateDTO;
import homework.backend.example.dto.review.rolereview.RoleReviewPatchDTO;
import homework.backend.example.dto.review.rolereview.RoleReviewPutDTO;
import homework.backend.example.dto.review.rolereview.RoleReviewReadDTO;
import homework.backend.example.service.RoleReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/reviews")
public class RoleReviewController {

    @Autowired
    private RoleReviewService reviewService;

    @GetMapping("/{id}")
    public RoleReviewReadDTO getReview(@PathVariable UUID id) {
        return reviewService.getReview(id);
    }

    @PostMapping
    public RoleReviewReadDTO createReview(@RequestBody RoleReviewCreateDTO create) {
        return reviewService.createReview(create);
    }

    @PatchMapping("/{id}")
    public RoleReviewReadDTO patchReview(@PathVariable UUID id, @RequestBody RoleReviewPatchDTO patch) {
        return reviewService.patchReview(id, patch);
    }

    @PutMapping("/{id}")
    public RoleReviewReadDTO updateReview(@PathVariable UUID id, @RequestBody RoleReviewPutDTO put) {
        return reviewService.updateReview(id, put);
    }

    @DeleteMapping("/{id}")
    public void deleteReview(@PathVariable UUID id) {
        reviewService.deleteReview(id);
    }
}
