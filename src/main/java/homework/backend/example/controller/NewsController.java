package homework.backend.example.controller;

import homework.backend.example.dto.news.NewsCreateDTO;
import homework.backend.example.dto.news.NewsPatchDTO;
import homework.backend.example.dto.news.NewsPutDTO;
import homework.backend.example.dto.news.NewsReadDTO;
import homework.backend.example.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/news")
public class NewsController {

    @Autowired
    private NewsService newsService;

    @GetMapping("/{id}")
    public NewsReadDTO getNews(@PathVariable UUID id) {
        return newsService.getNews(id);
    }

    @PostMapping
    public NewsReadDTO createNews(@RequestBody NewsCreateDTO create) {
        return newsService.createNews(create);
    }

    @PutMapping("/{id}")
    public NewsReadDTO putNews(@PathVariable UUID id, @RequestBody NewsPutDTO newsPut) {
        return newsService.upDateNews(id, newsPut);
    }

    @PatchMapping("/{id}")
    public NewsReadDTO patchNews(@PathVariable UUID id, @RequestBody NewsPatchDTO newsPut) {
        return newsService.patchNews(id, newsPut);
    }

    @DeleteMapping("/{id}")
    public void deleteNews(@PathVariable UUID id) {
        newsService.deleteNews(id);
    }
}
