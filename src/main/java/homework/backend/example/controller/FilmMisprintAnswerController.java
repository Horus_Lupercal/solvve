package homework.backend.example.controller;

import homework.backend.example.dto.misprintsignal.FilmMisprintSignalReadDTO;
import homework.backend.example.service.FilmMisprintSignalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/api/filmMisPrintAnswers")
public class FilmMisprintAnswerController {

    @Autowired
    private FilmMisprintSignalService filmMisprintSignalService;

    @PatchMapping
    public FilmMisprintSignalReadDTO fixFilm(@RequestBody FilmMisprintSignalReadDTO read) {
        return filmMisprintSignalService.approveFilmsMisPrintSignal(read);
    }
}
