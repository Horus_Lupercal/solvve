package homework.backend.example.controller;

import homework.backend.example.dto.film.FilmCreateDTO;
import homework.backend.example.dto.film.FilmPatchDTO;
import homework.backend.example.dto.film.FilmPutDTO;
import homework.backend.example.dto.film.FilmReadDTO;
import homework.backend.example.service.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/films")
public class FilmController {

    @Autowired
    private FilmService filmService;

    @GetMapping("/{id}")
    public FilmReadDTO getFilm(@PathVariable UUID id) {
        return filmService.getFilm(id);
    }

    @PostMapping
    public FilmReadDTO createFilm(@RequestBody FilmCreateDTO createDTO) {
        return filmService.createFilm(createDTO);
    }

    @PatchMapping("/{id}")
    public FilmReadDTO patchFilm(@PathVariable UUID id, @RequestBody FilmPatchDTO patch) {
        return filmService.patchFilm(id, patch);
    }

    @DeleteMapping("/{id}")
    public void deleteFilm(@PathVariable UUID id) {
        filmService.deleteFilm(id);
    }

    @PutMapping("/{id}")
    public FilmReadDTO putFilm(@PathVariable UUID id, @RequestBody FilmPutDTO put) {
        return filmService.upDateFilm(id, put);
    }
}
