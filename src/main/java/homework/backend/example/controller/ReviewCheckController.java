package homework.backend.example.controller;

import homework.backend.example.dto.reviewcheck.ReviewCheckCreateDTO;
import homework.backend.example.dto.reviewcheck.ReviewCheckPatchDTO;
import homework.backend.example.dto.reviewcheck.ReviewCheckPutDTO;
import homework.backend.example.dto.reviewcheck.ReviewCheckReadDTO;
import homework.backend.example.service.ReviewCheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/reviewChecks")
public class ReviewCheckController {

    @Autowired
    private ReviewCheckService reviewCheckService;

    @GetMapping("/{id}")
    public ReviewCheckReadDTO getReviewCheck(@PathVariable UUID id) {
        return reviewCheckService.getReviewCheck(id);
    }

    @PostMapping
    public ReviewCheckReadDTO createReviewCheck(@RequestBody ReviewCheckCreateDTO create) {
        return reviewCheckService.createReviewCheck(create);
    }

    @PatchMapping("/{id}")
    public ReviewCheckReadDTO patchReviewCheck(@PathVariable UUID id, @RequestBody ReviewCheckPatchDTO patch) {
        return reviewCheckService.patchReviewCheck(id, patch);
    }

    @PutMapping("/{id}")
    public ReviewCheckReadDTO updateReviewCheck(@PathVariable UUID id, @RequestBody ReviewCheckPutDTO put) {
        return reviewCheckService.updateReviewCheck(id, put);
    }

    @DeleteMapping("/{id}")
    public void deleteReviewCheck(@PathVariable UUID id) {
        reviewCheckService.deleteReviewCheck(id);
    }
}
