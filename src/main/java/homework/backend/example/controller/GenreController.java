package homework.backend.example.controller;

import homework.backend.example.dto.genre.GenreCreateDTO;
import homework.backend.example.dto.genre.GenrePatchDTO;
import homework.backend.example.dto.genre.GenrePutDTO;
import homework.backend.example.dto.genre.GenreReadDTO;
import homework.backend.example.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/genres")
public class GenreController {

    @Autowired
    private GenreService genreService;

    @GetMapping("/{id}")
    public GenreReadDTO getGenre(@PathVariable UUID id) {
        return genreService.getGenre(id);
    }

    @PostMapping
    public GenreReadDTO createGenre(@RequestBody GenreCreateDTO create) {
        return genreService.createGenre(create);
    }

    @PatchMapping("/{id}")
    public GenreReadDTO patchGenre(@PathVariable UUID id, @RequestBody GenrePatchDTO patch) {
        return genreService.patchGenre(id, patch);
    }

    @PutMapping("/{id}")
    public GenreReadDTO updateGenre(@PathVariable UUID id, @RequestBody GenrePutDTO put) {
        return genreService.updateGenre(id, put);
    }

    @DeleteMapping("/{id}")
    public void deleteGenre(@PathVariable UUID id) {
        genreService.deleteGenre(id);
    }
}
