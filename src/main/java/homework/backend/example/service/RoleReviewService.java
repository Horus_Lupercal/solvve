package homework.backend.example.service;

import homework.backend.example.domain.massages.review.RoleReview;
import homework.backend.example.dto.review.rolereview.RoleReviewCreateDTO;
import homework.backend.example.dto.review.rolereview.RoleReviewPatchDTO;
import homework.backend.example.dto.review.rolereview.RoleReviewPutDTO;
import homework.backend.example.dto.review.rolereview.RoleReviewReadDTO;
import homework.backend.example.repository.RepositoryHelper;
import homework.backend.example.repository.review.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class RoleReviewService {

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public RoleReviewReadDTO getReview(UUID id) {
        RoleReview review = repositoryHelper.getReference(RoleReview.class, id);
        return translationService.toRead(review);
    }

    public RoleReviewReadDTO createReview(RoleReviewCreateDTO create) {
        RoleReview review = translationService.toEntity(create);
        review = reviewRepository.save(review);
        return translationService.toRead(review);
    }

    public RoleReviewReadDTO patchReview(UUID id, RoleReviewPatchDTO patch) {
        RoleReview review = repositoryHelper.getReference(RoleReview.class, id);
        translationService.patchEntity(review, patch);
        review = reviewRepository.save(review);
        return translationService.toRead(review);
    }

    public RoleReviewReadDTO updateReview(UUID id, RoleReviewPutDTO put) {
        RoleReview review = repositoryHelper.getReference(RoleReview.class, id);
        translationService.updateEntity(review, put);
        review = reviewRepository.save(review);
        return translationService.toRead(review);
    }

    public void deleteReview(UUID id) {
        reviewRepository.delete(repositoryHelper.getReference(RoleReview.class, id));
    }
}
