package homework.backend.example.service;

import homework.backend.example.domain.content.Film;
import homework.backend.example.domain.content.Genre;
import homework.backend.example.domain.content.News;
import homework.backend.example.domain.content.Role;
import homework.backend.example.domain.ententies.ReviewCheck;
import homework.backend.example.domain.massages.errormassages.misprintsignal.FilmMisprintSignal;
import homework.backend.example.domain.massages.review.RoleReview;
import homework.backend.example.domain.persons.Actor;
import homework.backend.example.domain.persons.CrewMember;
import homework.backend.example.domain.persons.Person;
import homework.backend.example.domain.users.RegisteredUser;
import homework.backend.example.dto.actor.ActorCreateDTO;
import homework.backend.example.dto.actor.ActorPatchDTO;
import homework.backend.example.dto.actor.ActorPutDTO;
import homework.backend.example.dto.actor.ActorReadDTO;
import homework.backend.example.dto.crewmember.CrewMemberCreateDTO;
import homework.backend.example.dto.crewmember.CrewMemberPatchDTO;
import homework.backend.example.dto.crewmember.CrewMemberPutDTO;
import homework.backend.example.dto.crewmember.CrewMemberReadDTO;
import homework.backend.example.dto.film.FilmCreateDTO;
import homework.backend.example.dto.film.FilmPatchDTO;
import homework.backend.example.dto.film.FilmPutDTO;
import homework.backend.example.dto.film.FilmReadDTO;
import homework.backend.example.dto.genre.GenreCreateDTO;
import homework.backend.example.dto.genre.GenrePatchDTO;
import homework.backend.example.dto.genre.GenrePutDTO;
import homework.backend.example.dto.genre.GenreReadDTO;
import homework.backend.example.dto.misprintsignal.FilmMisprintSignalCreateDTO;
import homework.backend.example.dto.misprintsignal.FilmMisprintSignalPatchDTO;
import homework.backend.example.dto.misprintsignal.FilmMisprintSignalPutDTO;
import homework.backend.example.dto.misprintsignal.FilmMisprintSignalReadDTO;
import homework.backend.example.dto.news.NewsCreateDTO;
import homework.backend.example.dto.news.NewsPatchDTO;
import homework.backend.example.dto.news.NewsPutDTO;
import homework.backend.example.dto.news.NewsReadDTO;
import homework.backend.example.dto.person.PersonCreateDTO;
import homework.backend.example.dto.person.PersonPatchDTO;
import homework.backend.example.dto.person.PersonPutDTO;
import homework.backend.example.dto.person.PersonReadDTO;
import homework.backend.example.dto.registreduser.RegisteredUserCreateDTO;
import homework.backend.example.dto.registreduser.RegisteredUserPatchDTO;
import homework.backend.example.dto.registreduser.RegisteredUserPutDTO;
import homework.backend.example.dto.registreduser.RegisteredUserReadDTO;
import homework.backend.example.dto.review.rolereview.RoleReviewCreateDTO;
import homework.backend.example.dto.review.rolereview.RoleReviewPatchDTO;
import homework.backend.example.dto.review.rolereview.RoleReviewPutDTO;
import homework.backend.example.dto.review.rolereview.RoleReviewReadDTO;
import homework.backend.example.dto.reviewcheck.ReviewCheckCreateDTO;
import homework.backend.example.dto.reviewcheck.ReviewCheckPatchDTO;
import homework.backend.example.dto.reviewcheck.ReviewCheckPutDTO;
import homework.backend.example.dto.reviewcheck.ReviewCheckReadDTO;
import homework.backend.example.dto.role.*;
import homework.backend.example.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TranslationService {

    @Autowired
    private RepositoryHelper repositoryHelper;

    public FilmMisprintSignal toEntity(FilmMisprintSignalCreateDTO create) {
        FilmMisprintSignal filmMisprintSignal = new FilmMisprintSignal();
        filmMisprintSignal.setFilm(repositoryHelper.getReference(Film.class, create.getFilmId()));
        filmMisprintSignal.setUser(repositoryHelper.getRequired(RegisteredUser.class, create.getUserId()));
        filmMisprintSignal.setCorrectString(create.getCorrectString());
        filmMisprintSignal.setWrongString(create.getWrongString());
        filmMisprintSignal.setVerification(create.getVerification());
        return filmMisprintSignal;
    }

    public Film toEntity(FilmCreateDTO createDTO) {
        Film film = new Film();
        film.setTitle(createDTO.getTitle());
        film.setDescription(createDTO.getDescription());
        film.setBudget(createDTO.getBudget());
        film.setRelease(createDTO.getRelease());
        film.setFees(createDTO.getFees());
        film.setStatus(createDTO.getStatus());
        film.setAverageMark(createDTO.getAverageMark());
        return film;
    }

    public Genre toEntity(GenreCreateDTO create) {
        Genre genre = new Genre();
        genre.setGenreType(create.getGenreType());
        genre.setDescription(create.getDescription());
        return genre;
    }

    public Actor toEntity(ActorCreateDTO create) {
        Actor actor = new Actor();
        actor.setMiddlePay(create.getMiddlePay());
        actor.setPerson(repositoryHelper.getReference(Person.class, create.getPersonId()));
        return actor;
    }

    public RegisteredUser toEntity(RegisteredUserCreateDTO create) {
        RegisteredUser user = new RegisteredUser();
        user.setSex(create.getSex());
        user.setName(create.getName());
        user.setEmail(create.getEmail());
        user.setPassword(create.getPassword());
        user.setTrustLevel(create.getTrustLevel());
        user.setAccessType(create.getAccessType());
        user.setPostType(create.getPostType());
        return user;
    }

    public News toEntity(NewsCreateDTO create) {
        News news = new News();
        news.setText(create.getText());
        return news;
    }

    public Person toEntity(PersonCreateDTO createDTO) {
        Person person = new Person();
        person.setName(createDTO.getName());
        person.setBiography(createDTO.getBiography());
        person.setBirthday(createDTO.getBirthday());
        person.setSex(createDTO.getSex());
        return person;
    }

    public Role toEntity(RoleCreateDTO create) {
        Role role = new Role();
        role.setActor(repositoryHelper.getReference(Actor.class, create.getActorId()));
        role.setFilm(repositoryHelper.getReference(Film.class, create.getFilmId()));
        role.setDescription(create.getDescription());
        role.setPay(create.getPay());
        role.setAverageMark(create.getAverageMark());
        return role;
    }

    public CrewMember toEntity(CrewMemberCreateDTO create) {
        CrewMember crewMember = new CrewMember();
        crewMember.setPost(create.getPost());
        crewMember.setPerson(repositoryHelper.getReference(Person.class, create.getPersonId()));
        crewMember.setFilm(repositoryHelper.getReference(Film.class, create.getFilmId()));
        return crewMember;
    }

    public ReviewCheck toEntity(ReviewCheckCreateDTO create) {
        ReviewCheck reviewCheck = new ReviewCheck();
        reviewCheck.setCheckStatus(create.getCheckStatus());
        reviewCheck.setModerator(repositoryHelper.getReference(RegisteredUser.class, create.getModeratorId()));
        return reviewCheck;
    }

    public RoleReview toEntity(RoleReviewCreateDTO create) {
        RoleReview review = new RoleReview();
        review.setVerification(create.getVerification());
        review.setReview(create.getReview());
        review.setRole(repositoryHelper.getReference(Role.class, create.getRoleId()));
        review.setUser(repositoryHelper.getReference(RegisteredUser.class, create.getUserId()));
        return review;
    }

    public void patchEntity(FilmMisprintSignal filmMisprintSignal, FilmMisprintSignalPatchDTO patch) {
        if (patch.getFilmId() != null) {
            filmMisprintSignal.setFilm(repositoryHelper.getReference(Film.class, patch.getFilmId()));
        }
        if (patch.getCorrectString() != null) {
            filmMisprintSignal.setCorrectString(patch.getCorrectString());
        }
        if (patch.getWrongString() != null) {
            filmMisprintSignal.setWrongString(patch.getWrongString());
        }
        if (patch.getVerification() != null) {
            filmMisprintSignal.setVerification(patch.getVerification());
        }
    }

    public void patchEntity(RoleReview review, RoleReviewPatchDTO patch) {
        if (patch.getReview() != null) {
            review.setReview(patch.getReview());
        }
        if (patch.getReviewCheckId() != null) {
            review.setReviewCheck(repositoryHelper.getReference(ReviewCheck.class, patch.getReviewCheckId()));
        }
        if (patch.getRoleId() != null) {
            review.setRole(repositoryHelper.getReference(Role.class, patch.getRoleId()));
        }
        if (patch.getVerification() != null) {
            review.setVerification(patch.getVerification());
        }
    }

    public void patchEntity(ReviewCheck reviewCheck, ReviewCheckPatchDTO patch) {
        if (patch.getCheckStatus() != null) {
            reviewCheck.setCheckStatus(patch.getCheckStatus());
        }
        if (patch.getModeratorId() != null) {
            reviewCheck.setModerator(repositoryHelper.getReference(RegisteredUser.class, patch.getModeratorId()));
        }
    }

    public void patchEntity(Film film, FilmPatchDTO patch) {
        if (patch.getTitle() != null) {
            film.setTitle(patch.getTitle());
        }
        if (patch.getDescription() != null) {
            film.setDescription(patch.getDescription());
        }
        if (patch.getBudget() != null) {
            film.setBudget(patch.getBudget());
        }
        if (patch.getRelease() != null) {
            film.setRelease(patch.getRelease());
        }
        if (patch.getFees() != null) {
            film.setFees(patch.getFees());
        }
        if (patch.getStatus() != null) {
            film.setStatus(patch.getStatus());
        }
        if (patch.getAverageMark() != null) {
            film.setAverageMark(patch.getAverageMark());
        }
    }

    public void patchEntity(Person person, PersonPatchDTO patch) {
        if (patch.getName() != null) {
            person.setName(patch.getName());
        }
        if (patch.getBirthday() != null) {
            person.setBirthday(patch.getBirthday());
        }
        if (patch.getBiography() != null) {
            person.setBiography(patch.getBiography());
        }
        if (patch.getSex() != null) {
            person.setSex(person.getSex());
        }
    }

    public void patchEntity(News news, NewsPatchDTO patch) {
        if (patch.getText() != null) {
            news.setText(patch.getText());
        }
    }

    public void patchEntity(Role role, RolePatchDTO patch) {
        if (patch.getDescription() != null) {
            role.setDescription(patch.getDescription());
        }
        if (patch.getPay() != null) {
            role.setPay(patch.getPay());
        }
        if (patch.getAverageMark() != null) {
            role.setAverageMark(patch.getAverageMark());
        }
        if (patch.getActorId() != null) {
            role.setActor(repositoryHelper.getReference(Actor.class, patch.getActorId()));
        }
        if (patch.getFilmId() != null) {
            role.setFilm(repositoryHelper.getReference(Film.class, patch.getFilmId()));
        }
    }

    public void patchEntity(Actor actor, ActorPatchDTO patch) {
        if (patch.getMiddlePay() != null) {
            actor.setMiddlePay(patch.getMiddlePay());
        }
        if (patch.getPersonId() != null) {
            actor.setPerson(repositoryHelper.getReference(Person.class, patch.getPersonId()));
        }
    }

    public void patchEntity(RegisteredUser user, RegisteredUserPatchDTO patch) {
        if (patch.getName() != null) {
            user.setName(patch.getName());
        }
        if (patch.getEmail() != null) {
            user.setEmail(patch.getEmail());
        }
        if (patch.getPassword() != null) {
            user.setPassword(patch.getPassword());
        }
        if (patch.getSex() != null) {
            user.setSex(patch.getSex());
        }
        if (patch.getAccessType() != null) {
            user.setAccessType(patch.getAccessType());
        }
        if (patch.getPostType() != null) {
            user.setPostType(patch.getPostType());
        }
        if (patch.getTrustLevel() != null) {
            user.setTrustLevel(patch.getTrustLevel());
        }
    }

    public void patchEntity(CrewMember crewMember, CrewMemberPatchDTO patch) {
        if (patch.getPost() != null) {
            crewMember.setPost(patch.getPost());
        }
    }

    public void patchEntity(Genre genre, GenrePatchDTO patch) {
        if (patch.getDescription() != null) {
            genre.setDescription(patch.getDescription());
        }
        if (patch.getGenreType() != null) {
            genre.setGenreType(patch.getGenreType());
        }
    }

    public void updateEntity(FilmMisprintSignal filmMisprintSignal, FilmMisprintSignalPutDTO put) {
        filmMisprintSignal.setVerification(put.getVerification());
        filmMisprintSignal.setFilm(repositoryHelper.getReference(Film.class, put.getFilmId()));
        filmMisprintSignal.setCorrectString(put.getCorrectString());
        filmMisprintSignal.setWrongString(put.getWrongString());
    }

    public void updateEntity(ReviewCheck reviewCheck, ReviewCheckPutDTO put) {
        reviewCheck.setCheckStatus(put.getCheckStatus());
        reviewCheck.setModerator(repositoryHelper.getReference(RegisteredUser.class, put.getModeratorId()));
    }

    public void updateEntity(RegisteredUser user, RegisteredUserPutDTO put) {
        user.setName(put.getName());
        user.setEmail(put.getEmail());
        user.setPassword(put.getPassword());
        user.setSex(put.getSex());
        user.setAccessType(put.getAccessType());
        user.setPostType(put.getPostType());
        user.setTrustLevel(put.getTrustLevel());
    }

    public void updateEntity(Role role, RolePutDTO put) {
        role.setDescription(put.getDescription());
        role.setPay(put.getPay());
        role.setAverageMark(put.getAverageMark());
        role.setActor(repositoryHelper.getReference(Actor.class, put.getActorId()));
        role.setFilm(repositoryHelper.getReference(Film.class, put.getFilmId()));
    }

    public void updateEntity(News news, NewsPutDTO newsPutDTO) {
        news.setText(newsPutDTO.getText());
    }

    public void updateEntity(Person person, PersonPutDTO putDTO) {
        person.setName(putDTO.getName());
        person.setBirthday(putDTO.getBirthday());
        person.setBiography(putDTO.getBiography());
        person.setSex(putDTO.getSex());
    }

    public void updateEntity(Film film, FilmPutDTO putDTO) {
        film.setTitle(putDTO.getTitle());
        film.setBudget(putDTO.getBudget());
        film.setDescription(putDTO.getDescription());
        film.setRelease(putDTO.getRelease());
        film.setFees(putDTO.getFees());
        film.setStatus(putDTO.getStatus());
        film.setAverageMark(putDTO.getAverageMark());
    }

    public void updateEntity(Actor actor, ActorPutDTO put) {
        actor.setMiddlePay(put.getMiddlePay());
        actor.setPerson(repositoryHelper.getReference(Person.class, put.getPersonId()));
    }

    public void updateEntity(CrewMember crewMember, CrewMemberPutDTO put) {
        crewMember.setPost(put.getPost());
    }

    public void updateEntity(Genre genre, GenrePutDTO put) {
        genre.setDescription(put.getDescription());
        genre.setGenreType(put.getGenreType());
    }

    public void updateEntity(RoleReview review, RoleReviewPutDTO put) {
        review.setRole(repositoryHelper.getReference(Role.class, put.getRoleId()));
        review.setVerification(put.getVerification());
        review.setReview(put.getReview());
        review.setReviewCheck(repositoryHelper.getReference(ReviewCheck.class, put.getReviewCheckId()));
    }

    public RoleReadExtendedDTO toReadExt(Role role) {
        RoleReadExtendedDTO read = new RoleReadExtendedDTO();
        read.setId(role.getId());
        read.setActor(toRead(role.getActor()));
        read.setFilm(toRead(role.getFilm()));
        read.setPay(role.getPay());
        read.setAverageMark(role.getAverageMark());
        read.setDescription(role.getDescription());
        read.setCreatedAt(role.getCreatedAt());
        read.setUpdatedAt(role.getUpdatedAt());
        return read;
    }

    public FilmMisprintSignalReadDTO toRead(FilmMisprintSignal filmMisprintSignal) {
        FilmMisprintSignalReadDTO read = new FilmMisprintSignalReadDTO();
        read.setId(filmMisprintSignal.getId());
        read.setContentManagerId(filmMisprintSignal.getContentManager().getId());
        read.setCorrectString(filmMisprintSignal.getCorrectString());
        read.setCreatedAt(filmMisprintSignal.getCreatedAt());
        read.setFilmId(filmMisprintSignal.getFilm().getId());
        read.setFixDate(filmMisprintSignal.getFixDate());
        read.setUpdatedAt(filmMisprintSignal.getUpdatedAt());
        read.setUserId(filmMisprintSignal.getUser().getId());
        read.setWrongString(filmMisprintSignal.getWrongString());
        read.setVerification(filmMisprintSignal.getVerification());
        return read;
    }

    public FilmReadDTO toRead(Film film) {
        FilmReadDTO read = new FilmReadDTO();
        read.setId(film.getId());
        read.setTitle(film.getTitle());
        read.setBudget(film.getBudget());
        read.setDescription(film.getDescription());
        read.setRelease(film.getRelease());
        read.setFees(film.getFees());
        read.setStatus(film.getStatus());
        read.setAverageMark(film.getAverageMark());
        read.setCreatedAt(film.getCreatedAt());
        read.setUpdatedAt(film.getUpdatedAt());
        return read;
    }

    public RoleReadDTO toRead(Role role) {
        RoleReadDTO read = new RoleReadDTO();
        read.setId(role.getId());
        read.setActorId(role.getActor().getId());
        read.setFilmId(role.getFilm().getId());
        read.setAverageMark(role.getAverageMark());
        read.setPay(role.getPay());
        read.setDescription(role.getDescription());
        read.setCreatedAt(role.getCreatedAt());
        read.setUpdatedAt(role.getUpdatedAt());
        return read;
    }

    public ActorReadDTO toRead(Actor actor) {
        ActorReadDTO read = new ActorReadDTO();
        read.setId(actor.getId());
        read.setMiddlePay(actor.getMiddlePay());
        read.setPersonId(actor.getPerson().getId());
        read.setCreatedAt(actor.getCreatedAt());
        read.setUpdatedAt(actor.getUpdatedAt());
        return read;
    }

    public RegisteredUserReadDTO toRead(RegisteredUser user) {
        RegisteredUserReadDTO read = new RegisteredUserReadDTO();
        read.setId(user.getId());
        read.setSex(user.getSex());
        read.setName(user.getName());
        read.setEmail(user.getEmail());
        read.setPassword(user.getPassword());
        read.setPostType(user.getPostType());
        read.setAccessType(user.getAccessType());
        read.setTrustLevel(user.getTrustLevel());
        read.setUpdatedAt(user.getUpdatedAt());
        read.setCreatedAt(user.getCreatedAt());
        return read;
    }

    public PersonReadDTO toRead(Person person) {
        PersonReadDTO read = new PersonReadDTO();
        read.setId(person.getId());
        read.setName(person.getName());
        read.setBiography(person.getBiography());
        read.setBirthday(person.getBirthday());
        read.setSex(person.getSex());
        read.setCreatedAt(person.getCreatedAt());
        read.setUpdatedAt(person.getUpdatedAt());
        return read;
    }

    public NewsReadDTO toRead(News news) {
        NewsReadDTO read = new NewsReadDTO();
        read.setId(news.getId());
        read.setText(news.getText());
        read.setCreatedAt(news.getCreatedAt());
        read.setUpdatedAt(news.getUpdatedAt());
        return read;
    }

    public CrewMemberReadDTO toRead(CrewMember crewMember) {
        CrewMemberReadDTO read = new CrewMemberReadDTO();
        read.setId(crewMember.getId());
        read.setPost(crewMember.getPost());
        read.setFilmId(crewMember.getFilm().getId());
        read.setPersonId(crewMember.getPerson().getId());
        read.setCreatedAt(crewMember.getCreatedAt());
        read.setUpdatedAt(crewMember.getUpdatedAt());
        return read;
    }

    public GenreReadDTO toRead(Genre genre) {
        GenreReadDTO read = new GenreReadDTO();
        read.setId(genre.getId());
        read.setDescription(genre.getDescription());
        read.setGenreType(genre.getGenreType());
        read.setCreatedAt(genre.getCreatedAt());
        read.setUpdatedAt(genre.getUpdatedAt());
        return read;
    }

    public ReviewCheckReadDTO toRead(ReviewCheck reviewCheck) {
        ReviewCheckReadDTO read = new ReviewCheckReadDTO();
        read.setId(reviewCheck.getId());
        read.setCheckStatus(reviewCheck.getCheckStatus());
        read.setModeratorId(reviewCheck.getModerator().getId());
        read.setCreatedAt(reviewCheck.getCreatedAt());
        read.setUpdatedAt(reviewCheck.getUpdatedAt());
        return read;
    }

    public RoleReviewReadDTO toRead(RoleReview review) {
        RoleReviewReadDTO read = new RoleReviewReadDTO();
        read.setId(review.getId());
        read.setReview(review.getReview());
        read.setUserId(review.getUser().getId());
        read.setRoleId(review.getRole().getId());
        read.setReviewCheckId(review.getReviewCheck().getId());
        read.setVerification(review.getVerification());
        read.setCreatedAt(review.getCreatedAt());
        read.setUpdatedAt(review.getUpdatedAt());
        return read;
    }

    public void fixFilm(Film film, FilmMisprintSignalReadDTO read) {
        film.setDescription(film.getDescription().replace(read.getWrongString(), read.getCorrectString()));
    }
}
