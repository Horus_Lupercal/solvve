package homework.backend.example.service;

import homework.backend.example.domain.content.Role;
import homework.backend.example.dto.role.*;
import homework.backend.example.repository.RepositoryHelper;
import homework.backend.example.repository.rating.RatingRepository;
import homework.backend.example.repository.role.RoleRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Slf4j
public class RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RatingRepository ratingRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public RoleReadExtendedDTO getRoleExt(UUID id) {
        Role role = repositoryHelper.getRequired(Role.class, id);
        return translationService.toReadExt(role);
    }

    public List<RoleReadDTO> getRoles(RoleFilter filter) {
        List<Role> roles = roleRepository.findByFilter(filter);
        return roles.stream().map(translationService::toRead).collect(Collectors.toList());
    }

    public RoleReadDTO getRole(UUID id) {
        Role role = repositoryHelper.getRequired(Role.class, id);
        return translationService.toRead(role);
    }

    public void deleteRole(UUID id) {
        roleRepository.delete(repositoryHelper.getRequired(Role.class, id));
    }

    public RoleReadDTO createRole(RoleCreateDTO create) {
        Role role = translationService.toEntity(create);
        role = roleRepository.save(role);
        return translationService.toRead(role);
    }

    public RoleReadDTO upDateRole(UUID id, RolePutDTO put) {
        Role role = repositoryHelper.getRequired(Role.class, id);
        translationService.updateEntity(role, put);
        role = roleRepository.save(role);
        return translationService.toRead(role);
    }

    public RoleReadDTO patchRole(UUID id, RolePatchDTO patch) {
        Role role = repositoryHelper.getRequired(Role.class, id);
        translationService.patchEntity(role, patch);
        role = roleRepository.save(role);
        return translationService.toRead(role);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateAverageMarkOfRole(UUID roleId) {
        Double averageMark = ratingRepository.calcAverageMarkOfRole(roleId);
        Role role = repositoryHelper.getRequired(Role.class, roleId);

        log.info("Setting new average mark of role: {}. Old value: {}, new value: {}", roleId,
                role.getAverageMark(), averageMark);
        role.setAverageMark(averageMark);
        roleRepository.save(role);
    }
}
