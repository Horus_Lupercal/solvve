package homework.backend.example.service;

import homework.backend.example.domain.content.Genre;
import homework.backend.example.dto.genre.GenreCreateDTO;
import homework.backend.example.dto.genre.GenrePatchDTO;
import homework.backend.example.dto.genre.GenrePutDTO;
import homework.backend.example.dto.genre.GenreReadDTO;
import homework.backend.example.repository.RepositoryHelper;
import homework.backend.example.repository.genre.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class GenreService {

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public GenreReadDTO getGenre(UUID id) {
        Genre genre = repositoryHelper.getRequired(Genre.class, id);
        return translationService.toRead(genre);
    }

    public GenreReadDTO createGenre(GenreCreateDTO create) {
        Genre genre = translationService.toEntity(create);
        genre = genreRepository.save(genre);
        return translationService.toRead(genre);
    }

    public GenreReadDTO patchGenre(UUID id, GenrePatchDTO patch) {
        Genre genre = repositoryHelper.getRequired(Genre.class, id);
        translationService.patchEntity(genre, patch);
        genre = genreRepository.save(genre);
        return translationService.toRead(genre);
    }

    public GenreReadDTO updateGenre(UUID id, GenrePutDTO put) {
        Genre genre = repositoryHelper.getRequired(Genre.class, id);
        translationService.updateEntity(genre, put);
        genre = genreRepository.save(genre);
        return translationService.toRead(genre);
    }

    public void deleteGenre(UUID id) {
        genreRepository.delete(repositoryHelper.getRequired(Genre.class, id));
    }
}
