package homework.backend.example.service;

import homework.backend.example.domain.content.Film;
import homework.backend.example.dto.film.FilmCreateDTO;
import homework.backend.example.dto.film.FilmPatchDTO;
import homework.backend.example.dto.film.FilmPutDTO;
import homework.backend.example.dto.film.FilmReadDTO;
import homework.backend.example.dto.misprintsignal.FilmMisprintSignalReadDTO;
import homework.backend.example.repository.RepositoryHelper;
import homework.backend.example.repository.film.FilmRepository;
import homework.backend.example.repository.rating.RatingRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Slf4j
@Service
public class FilmService {

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private RatingRepository ratingRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public void deleteFilm(UUID id) {
        filmRepository.delete(repositoryHelper.getRequired(Film.class, id));
    }

    public FilmReadDTO patchFilm(UUID id, FilmPatchDTO patch) {
        Film film = repositoryHelper.getRequired(Film.class, id);
        translationService.patchEntity(film, patch);
        film = filmRepository.save(film);
        return translationService.toRead(film);
    }

    public FilmReadDTO getFilm(UUID id) {
        Film film = repositoryHelper.getRequired(Film.class, id);
        return translationService.toRead(film);
    }

    public FilmReadDTO createFilm(FilmCreateDTO create) {
        Film film = translationService.toEntity(create);
        film = filmRepository.save(film);
        return translationService.toRead(film);
    }

    public FilmReadDTO upDateFilm(UUID id, FilmPutDTO put) {
        Film film = repositoryHelper.getRequired(Film.class, id);
        translationService.updateEntity(film, put);
        film = filmRepository.save(film);
        return translationService.toRead(film);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateAverageMarkOfFilm(UUID filmId) {
        Double averageMark = ratingRepository.calcAverageMarkOfFilm(filmId);
        Film film = repositoryHelper.getRequired(Film.class, filmId);

        log.info("Setting new average mark of film: {}. Old value: {}, new value: {}", filmId,
                film.getAverageMark(), averageMark);
        film.setAverageMark(averageMark);
        filmRepository.save(film);
    }

    public FilmReadDTO fixFilm(FilmMisprintSignalReadDTO read) {
        Film film = repositoryHelper.getRequired(Film.class, read.getFilmId());
        translationService.fixFilm(film, read);
        film = filmRepository.save(film);
        return translationService.toRead(film);
    }
}
