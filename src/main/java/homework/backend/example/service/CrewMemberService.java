package homework.backend.example.service;

import homework.backend.example.domain.persons.CrewMember;
import homework.backend.example.dto.crewmember.CrewMemberCreateDTO;
import homework.backend.example.dto.crewmember.CrewMemberPatchDTO;
import homework.backend.example.dto.crewmember.CrewMemberPutDTO;
import homework.backend.example.dto.crewmember.CrewMemberReadDTO;
import homework.backend.example.repository.RepositoryHelper;
import homework.backend.example.repository.crewmember.CrewMemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class CrewMemberService {

    @Autowired
    private CrewMemberRepository crewMemberRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public CrewMemberReadDTO getCrewMember(UUID id) {
        CrewMember crewMember = repositoryHelper.getRequired(CrewMember.class, id);
        return translationService.toRead(crewMember);
    }

    public CrewMemberReadDTO createCrewMember(CrewMemberCreateDTO create) {
        CrewMember crewMember = translationService.toEntity(create);
        crewMember = crewMemberRepository.save(crewMember);
        return translationService.toRead(crewMember);
    }

    public CrewMemberReadDTO patchCrewMember(UUID id, CrewMemberPatchDTO patch) {
        CrewMember crewMember = repositoryHelper.getRequired(CrewMember.class, id);
        translationService.patchEntity(crewMember, patch);
        crewMember = crewMemberRepository.save(crewMember);
        return translationService.toRead(crewMember);
    }

    public CrewMemberReadDTO updateCrewMember(UUID id, CrewMemberPutDTO put) {
        CrewMember crewMember = repositoryHelper.getRequired(CrewMember.class, id);
        translationService.updateEntity(crewMember, put);
        crewMember = crewMemberRepository.save(crewMember);
        return translationService.toRead(crewMember);
    }

    public void deleteCrewMember(UUID id) {
        crewMemberRepository.delete(repositoryHelper.getRequired(CrewMember.class, id));
    }
}
