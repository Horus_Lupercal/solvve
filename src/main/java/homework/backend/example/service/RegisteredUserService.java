package homework.backend.example.service;

import homework.backend.example.domain.users.RegisteredUser;
import homework.backend.example.dto.registreduser.RegisteredUserCreateDTO;
import homework.backend.example.dto.registreduser.RegisteredUserPatchDTO;
import homework.backend.example.dto.registreduser.RegisteredUserPutDTO;
import homework.backend.example.dto.registreduser.RegisteredUserReadDTO;
import homework.backend.example.repository.RepositoryHelper;
import homework.backend.example.repository.registereduser.RegisteredUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class RegisteredUserService {

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public RegisteredUserReadDTO getUser(UUID id) {
        RegisteredUser user = repositoryHelper.getRequired(RegisteredUser.class, id);
        return translationService.toRead(user);
    }

    public RegisteredUserReadDTO createUser(RegisteredUserCreateDTO create) {
        RegisteredUser user = translationService.toEntity(create);
        user = registeredUserRepository.save(user);
        return translationService.toRead(user);
    }

    public RegisteredUserReadDTO patchUser(UUID id, RegisteredUserPatchDTO patch) {
        RegisteredUser user = repositoryHelper.getRequired(RegisteredUser.class, id);
        translationService.patchEntity(user, patch);
        user = registeredUserRepository.save(user);
        return translationService.toRead(user);
    }

    public RegisteredUserReadDTO upDateUser(UUID id, RegisteredUserPutDTO put) {
        RegisteredUser user = repositoryHelper.getRequired(RegisteredUser.class, id);
        translationService.updateEntity(user, put);
        user = registeredUserRepository.save(user);
        return translationService.toRead(user);
    }

    public void deleteUser(UUID id) {
        registeredUserRepository.delete(repositoryHelper.getRequired(RegisteredUser.class, id));
    }
}
