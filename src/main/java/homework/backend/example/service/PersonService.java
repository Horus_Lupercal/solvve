package homework.backend.example.service;

import homework.backend.example.domain.persons.Person;
import homework.backend.example.dto.person.PersonCreateDTO;
import homework.backend.example.dto.person.PersonPatchDTO;
import homework.backend.example.dto.person.PersonPutDTO;
import homework.backend.example.dto.person.PersonReadDTO;
import homework.backend.example.repository.RepositoryHelper;
import homework.backend.example.repository.person.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public PersonReadDTO getPerson(UUID id) {
        Person person = repositoryHelper.getRequired(Person.class, id);
        return translationService.toRead(person);
    }

    public PersonReadDTO createPerson(PersonCreateDTO createDTO) {
        Person person = translationService.toEntity(createDTO);
        person = personRepository.save(person);
        return translationService.toRead(person);
    }

    public PersonReadDTO patchPerson(UUID id, PersonPatchDTO patch) {
        Person person = repositoryHelper.getRequired(Person.class, id);
        translationService.patchEntity(person, patch);
        person = personRepository.save(person);
        return translationService.toRead(person);
    }

    public PersonReadDTO upDatePerson(UUID id, PersonPutDTO put) {
        Person person = repositoryHelper.getRequired(Person.class, id);
        translationService.updateEntity(person, put);
        person = personRepository.save(person);
        return translationService.toRead(person);
    }

    public void deletePerson(UUID id) {
        personRepository.delete(repositoryHelper.getRequired(Person.class, id));
    }
}
