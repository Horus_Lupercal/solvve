package homework.backend.example.service;

import homework.backend.example.domain.content.News;
import homework.backend.example.dto.news.NewsCreateDTO;
import homework.backend.example.dto.news.NewsPatchDTO;
import homework.backend.example.dto.news.NewsPutDTO;
import homework.backend.example.dto.news.NewsReadDTO;
import homework.backend.example.repository.RepositoryHelper;
import homework.backend.example.repository.news.NewsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class NewsService {

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public NewsReadDTO getNews(UUID id) {
        News news = repositoryHelper.getRequired(News.class, id);
        return translationService.toRead(news);
    }

    public NewsReadDTO createNews(NewsCreateDTO create) {
        News news = translationService.toEntity(create);
        news = newsRepository.save(news);
        return translationService.toRead(news);
    }

    public NewsReadDTO upDateNews(UUID id, NewsPutDTO newsPut) {
        News news = repositoryHelper.getRequired(News.class, id);
        translationService.updateEntity(news, newsPut);
        news = newsRepository.save(news);
        return translationService.toRead(news);
    }

    public NewsReadDTO patchNews(UUID id, NewsPatchDTO patchDTO) {
        News news = repositoryHelper.getRequired(News.class, id);
        translationService.patchEntity(news, patchDTO);
        news = newsRepository.save(news);
        return translationService.toRead(news);
    }

    public void deleteNews(UUID id) {
        newsRepository.delete(repositoryHelper.getRequired(News.class, id));
    }
}
