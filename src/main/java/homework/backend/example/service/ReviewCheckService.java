package homework.backend.example.service;

import homework.backend.example.domain.ententies.ReviewCheck;
import homework.backend.example.dto.reviewcheck.ReviewCheckCreateDTO;
import homework.backend.example.dto.reviewcheck.ReviewCheckPatchDTO;
import homework.backend.example.dto.reviewcheck.ReviewCheckPutDTO;
import homework.backend.example.dto.reviewcheck.ReviewCheckReadDTO;
import homework.backend.example.repository.RepositoryHelper;
import homework.backend.example.repository.reviewcheck.ReviewCheckRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ReviewCheckService {

    @Autowired
    private ReviewCheckRepository reviewCheckRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    public ReviewCheckReadDTO getReviewCheck(UUID id) {
        ReviewCheck reviewCheck = repositoryHelper.getRequired(ReviewCheck.class, id);
        return translationService.toRead(reviewCheck);
    }

    public ReviewCheckReadDTO createReviewCheck(ReviewCheckCreateDTO create) {
        ReviewCheck reviewCheck = translationService.toEntity(create);
        reviewCheck = reviewCheckRepository.save(reviewCheck);
        return translationService.toRead(reviewCheck);
    }

    public ReviewCheckReadDTO patchReviewCheck(UUID id, ReviewCheckPatchDTO patch) {
        ReviewCheck reviewCheck = repositoryHelper.getRequired(ReviewCheck.class, id);
        translationService.patchEntity(reviewCheck, patch);
        reviewCheck = reviewCheckRepository.save(reviewCheck);
        return translationService.toRead(reviewCheck);
    }

    public ReviewCheckReadDTO updateReviewCheck(UUID id, ReviewCheckPutDTO put) {
        ReviewCheck reviewCheck = repositoryHelper.getRequired(ReviewCheck.class, id);
        translationService.updateEntity(reviewCheck, put);
        reviewCheck = reviewCheckRepository.save(reviewCheck);
        return translationService.toRead(reviewCheck);
    }

    public void deleteReviewCheck(UUID id) {
        reviewCheckRepository.delete(repositoryHelper.getRequired(ReviewCheck.class, id));
    }
}
