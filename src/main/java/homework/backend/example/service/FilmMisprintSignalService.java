package homework.backend.example.service;

import homework.backend.example.domain.enums.VerificationStatusType;
import homework.backend.example.domain.massages.errormassages.misprintsignal.FilmMisprintSignal;
import homework.backend.example.domain.users.RegisteredUser;
import homework.backend.example.dto.misprintsignal.FilmMisprintSignalCreateDTO;
import homework.backend.example.dto.misprintsignal.FilmMisprintSignalPatchDTO;
import homework.backend.example.dto.misprintsignal.FilmMisprintSignalPutDTO;
import homework.backend.example.dto.misprintsignal.FilmMisprintSignalReadDTO;
import homework.backend.example.exception.EntityStatusException;
import homework.backend.example.repository.RepositoryHelper;
import homework.backend.example.repository.errorsignal.ErrorSignalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Service
public class FilmMisprintSignalService {

    @Autowired
    private ErrorSignalRepository errorSignalRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private FilmService filmService;

    public void deleteFilmMisprintSignal(UUID id) {
        errorSignalRepository.delete(repositoryHelper.getRequired(FilmMisprintSignal.class, id));
    }

    public FilmMisprintSignalReadDTO patchFilmMisprintSignal(UUID id, FilmMisprintSignalPatchDTO patch) {
        FilmMisprintSignal filmMisprintSignal = repositoryHelper.getRequired(FilmMisprintSignal.class, id);
        translationService.patchEntity(filmMisprintSignal, patch);
        filmMisprintSignal = errorSignalRepository.save(filmMisprintSignal);
        return translationService.toRead(filmMisprintSignal);
    }

    public FilmMisprintSignalReadDTO getFilmMisprintSignal(UUID id) {
        FilmMisprintSignal filmMisprintSignal = repositoryHelper.getRequired(FilmMisprintSignal.class, id);
        return translationService.toRead(filmMisprintSignal);
    }

    public FilmMisprintSignalReadDTO createFilmMisprintSignal(FilmMisprintSignalCreateDTO create) {
        FilmMisprintSignal filmMisprintSignal = translationService.toEntity(create);
        filmMisprintSignal = errorSignalRepository.save(filmMisprintSignal);
        return translationService.toRead(filmMisprintSignal);
    }

    public FilmMisprintSignalReadDTO updateFilmMisprintSignal(UUID id, FilmMisprintSignalPutDTO put) {
        FilmMisprintSignal filmMisprintSignal = repositoryHelper.getRequired(FilmMisprintSignal.class, id);
        translationService.updateEntity(filmMisprintSignal, put);
        filmMisprintSignal = errorSignalRepository.save(filmMisprintSignal);
        return translationService.toRead(filmMisprintSignal);
    }

    public FilmMisprintSignalReadDTO approveFilmsMisPrintSignal(FilmMisprintSignalReadDTO read) {
        if (read.getVerification() != VerificationStatusType.NOT_CHECKED) {
            throw new EntityStatusException(FilmMisprintSignalReadDTO.class, read.getId());
        }
        FilmMisprintSignal filmMisprintSignal = repositoryHelper.getRequired(
                FilmMisprintSignal.class, read.getId());

        filmService.fixFilm(read);

        filmMisprintSignal.setVerification(VerificationStatusType.CHECKED);
        filmMisprintSignal.setUpdatedAt(Instant.now());

        filmMisprintSignal.setContentManager(
                repositoryHelper.getReference(RegisteredUser.class, read.getContentManagerId()));

        filmMisprintSignal = errorSignalRepository.save(filmMisprintSignal);
        approveAllTheSameFilmsMisPrintSignal(filmMisprintSignal);
        return translationService.toRead(filmMisprintSignal);
    }

    private void approveAllTheSameFilmsMisPrintSignal(FilmMisprintSignal misprint) {
        List<FilmMisprintSignal> misprintSignals = errorSignalRepository.findByFilmIdAndWrongString(
                misprint.getId(), misprint.getWrongString());
        if (misprintSignals.size() > 0) {
            for (FilmMisprintSignal filmSignal : misprintSignals) {
                filmSignal.setCorrectString(misprint.getCorrectString());
                filmSignal.setVerification(misprint.getVerification());
                errorSignalRepository.save(filmSignal);
            }
        }
    }
}
