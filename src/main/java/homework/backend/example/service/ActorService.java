package homework.backend.example.service;

import homework.backend.example.domain.persons.Actor;
import homework.backend.example.dto.actor.ActorCreateDTO;
import homework.backend.example.dto.actor.ActorPatchDTO;
import homework.backend.example.dto.actor.ActorPutDTO;
import homework.backend.example.dto.actor.ActorReadDTO;
import homework.backend.example.repository.RepositoryHelper;
import homework.backend.example.repository.actor.ActorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ActorService {

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;


    public ActorReadDTO getActor(UUID id) {
        Actor actor = repositoryHelper.getRequired(Actor.class, id);
        return translationService.toRead(actor);
    }

    public ActorReadDTO createActor(ActorCreateDTO create) {
        Actor actor = translationService.toEntity(create);
        actor = actorRepository.save(actor);
        return translationService.toRead(actor);
    }

    public ActorReadDTO patchActor(UUID id, ActorPatchDTO patch) {
        Actor actor = repositoryHelper.getRequired(Actor.class, id);
        translationService.patchEntity(actor, patch);
        actor = actorRepository.save(actor);
        return translationService.toRead(actor);
    }

    public ActorReadDTO upDateActor(UUID id, ActorPutDTO put) {
        Actor actor = repositoryHelper.getRequired(Actor.class, id);
        translationService.updateEntity(actor, put);
        actor = actorRepository.save(actor);
        return translationService.toRead(actor);
    }

    public void deleteActor(UUID id) {
        actorRepository.delete(repositoryHelper.getRequired(Actor.class, id));
    }
}
