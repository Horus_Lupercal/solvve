package homework.backend.example.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.UUID;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class EntityStatusException extends RuntimeException {
    public EntityStatusException(Class entityClass, UUID id) {
        super(String.format("Entity %s with id=%s has wrong status!", entityClass.getSimpleName(), id));
    }
}
