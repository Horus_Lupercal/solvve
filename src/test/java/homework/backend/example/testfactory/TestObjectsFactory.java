package homework.backend.example.testfactory;
/*
 * Евгений, это я пытался сделать класс который будет заполнять поля других классов, вышло не очень.
 * Получил от нее в некоторых местах больше гемора чем пользы.
 * Я уже прочитал что свитчи лучше не использовать, так как брейки усложняют понимание кода. в будущем поправлю.
 */

import homework.backend.example.domain.content.Film;
import homework.backend.example.domain.content.News;
import homework.backend.example.domain.content.Role;
import homework.backend.example.domain.ententies.rating.filmrating.FilmRating;
import homework.backend.example.domain.ententies.rating.rolerating.RoleRating;
import homework.backend.example.domain.enums.*;
import homework.backend.example.domain.massages.errormassages.misprintsignal.FilmMisprintSignal;
import homework.backend.example.domain.persons.Actor;
import homework.backend.example.domain.persons.CrewMember;
import homework.backend.example.domain.persons.Person;
import homework.backend.example.domain.users.RegisteredUser;
import homework.backend.example.dto.actor.ActorCreateDTO;
import homework.backend.example.dto.actor.ActorPatchDTO;
import homework.backend.example.dto.misprintsignal.FilmMisprintSignalPatchDTO;
import homework.backend.example.dto.misprintsignal.FilmMisprintSignalReadDTO;
import homework.backend.example.dto.role.RoleCreateDTO;
import homework.backend.example.dto.role.RolePatchDTO;
import homework.backend.example.dto.role.RolePutDTO;
import homework.backend.example.repository.actor.ActorRepository;
import homework.backend.example.repository.crewmember.CrewMemberRepository;
import homework.backend.example.repository.errorsignal.ErrorSignalRepository;
import homework.backend.example.repository.film.FilmRepository;
import homework.backend.example.repository.news.NewsRepository;
import homework.backend.example.repository.person.PersonRepository;
import homework.backend.example.repository.rating.RatingRepository;
import homework.backend.example.repository.registereduser.RegisteredUserRepository;
import homework.backend.example.repository.role.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

@Service
public class TestObjectsFactory {

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private RegisteredUserRepository userRepository;

    @Autowired
    private CrewMemberRepository crewMemberRepository;

    @Autowired
    private RatingRepository ratingRepository;

    @Autowired
    private ErrorSignalRepository errorSignalRepository;

    public void setFields(Object o) throws Exception{
        Class aClass = o.getClass();
        Field[] fields = aClass.getDeclaredFields();
        boolean isDTO = aClass.getSimpleName().endsWith("DTO");
        for (Field field : fields) {
            field.setAccessible(true);
            if (field.getType() == String.class) {
                setString(field, o);
            }
            if (field.getType().getSimpleName().endsWith("Type")) {
                setEnum(field, o);
            }
            if (field.getType() == Long.class) {
                setLong(field, o);
            }
            if (field.getType() == Boolean.class) {
                field.set(o, true);
            }
            if (field.getType() == Double.class) {
                field.set(o, 8.8);
            }
            if (field.getType() == LocalDate.class) {
                field.set(o, LocalDate.parse("1988-05-12"));
            }
            if (field.getType() == Instant.class && isDTO) {
                field.set(o, Instant.now());
            }
            if (field.getType() == UUID.class && isDTO) {
                field.set(o, UUID.randomUUID());
            }
        }
        if (!isDTO) {
            String repository = findRepository(o);
            saveObject(repository, o);
        }
    }

    private String findRepository(Object o) {
        String nameClass = o.getClass().getSimpleName();
        return nameClass.concat("Repository");
    }

    private void saveObject(String repository, Object o) {
        switch (repository) {
            case "FilmRepository":
                o = filmRepository.save((Film) o);
                break;
            case "RoleRepository":
                o = roleRepository.save((Role) o);
                break;
            case "PersonRepository":
                o = personRepository.save((Person) o);
                break;
            case "NewsRepository":
                o = newsRepository.save((News) o);
                break;
            case "ActorRepository":
                o = actorRepository.save((Actor) o);
                break;
            case "RegisteredUserRepository":
                o = userRepository.save((RegisteredUser) o);
                break;
            case "CrewMemberRepository":
                o = crewMemberRepository.save((CrewMember) o);
                break;
            case "FilmMisprintSignalRepository":
                    o = errorSignalRepository.save((FilmMisprintSignal) o);
        }
    }

    private void setEnum(Field field, Object o) throws IllegalAccessException {
        switch (field.getType().getSimpleName()) {
            case "CrewMemberPostType":
                field.set(o, CrewMemberPostType.PRODUCER);
                break;
            case "SexType":
                field.set(o, SexType.FEMALE);
                break;
            case "GenreType":
                field.set(o, GenreType.ACTION);
                break;
            case "ReasonType":
                field.set(o, ReasonType.CENSURE);
                break;
            case "ReviewCheckStatusType":
                field.set(o, ReviewCheckStatusType.OK);
                break;
            case "UserAccessType":
                field.set(o, UserAccessType.BAN);
                break;
            case "UserPostType":
                field.set(o, UserPostType.ADMIN);
                break;
            case "UserTrustLevelType":
                field.set(o, UserTrustLevelType.MEDIUM);
                break;
            case "VerificationStatusType":
                field.set(o, VerificationStatusType.NOT_CHECKED);
                break;
        }
    }

    private void setString(Field field, Object o) throws Exception {
        switch (field.getName()) {
            case "title":
                field.set(o, "die hard");
                break;
            case "description":
                field.set(o, "Text about...");
                break;
            case "name":
                field.set(o, "Ivan32");
                break;
            case "email":
                field.set(o, "test@gmail.com");
                break;
            case "password":
                field.set(o, "qwerty123");
                break;
            case "wrongString":
                field.set(o, "Text");
                break;
            case "correctString":
                field.set(o, "The Text");
                break;
            case "biography":
                field.set(o, "live...");
                break;
            case "text":
                field.set(o, "fddf...");
                break;
        }
    }

    private void setLong(Field field, Object o) throws Exception {
        switch (field.getName()) {
            case "pay":
                field.set(o, 1_000_000L);
                break;
            case "budget":
                field.set(o, 12_000_000L);
                break;
            case "fees":
                field.set(o, 25_000_000L);
                break;
        }
    }

    public Film getFilm() {
        Film film = new Film();
        film.setTitle("Die Hard");
        film.setDescription("Cool");
        film.setBudget(1_000_000L);
        film.setRelease(LocalDate.parse("1988-05-12"));
        film.setFees(12_000_000L);
        film.setStatus(true);
        film.setAverageMark(8.8);
        return film = filmRepository.save(film);
    }

    public Actor getActor(Person person) {
        Actor actor = new Actor();
        actor.setMiddlePay(1000L);
        actor.setPerson(person);
        return actor = actorRepository.save(actor);
    }

    public ActorCreateDTO getActorCreateDTO(UUID personId) {
        ActorCreateDTO create = new ActorCreateDTO();
        create.setMiddlePay(1000L);
        create.setPersonId(personId);
        return create;
    }

    public ActorPatchDTO getActorPatchDTO(UUID personId) {
        ActorPatchDTO patch = new ActorPatchDTO();
        patch.setMiddlePay(1000L);
        patch.setPersonId(personId);
        return patch;
    }

    public FilmMisprintSignal getFilmMisprintSignal(Film film, RegisteredUser user, VerificationStatusType verification) {
        FilmMisprintSignal filmMisprintSignal = new FilmMisprintSignal();
        filmMisprintSignal.setFilm(film);
        filmMisprintSignal.setWrongString("test");
        filmMisprintSignal.setCorrectString("true");
        filmMisprintSignal.setVerification(verification);
        filmMisprintSignal.setUser(user);
        filmMisprintSignal = errorSignalRepository.save(filmMisprintSignal);
        return filmMisprintSignal;
    }

    public FilmMisprintSignal getFilmMisprintSignal(Film film, RegisteredUser user, String wrongString) {
        FilmMisprintSignal filmMisprintSignal = new FilmMisprintSignal();
        filmMisprintSignal.setFilm(film);
        filmMisprintSignal.setWrongString(wrongString);
        filmMisprintSignal.setCorrectString("true");
        filmMisprintSignal.setVerification(VerificationStatusType.NOT_CHECKED);
        filmMisprintSignal.setUser(user);
        filmMisprintSignal = errorSignalRepository.save(filmMisprintSignal);
        return filmMisprintSignal;
    }

    public FilmMisprintSignal getFilmMisprintSignal(Film film, RegisteredUser user,
                                                    String wrongString, VerificationStatusType verificationStatusType) {
        FilmMisprintSignal filmMisprintSignal = new FilmMisprintSignal();
        filmMisprintSignal.setFilm(film);
        filmMisprintSignal.setWrongString(wrongString);
        filmMisprintSignal.setCorrectString("true");
        filmMisprintSignal.setVerification(verificationStatusType);
        filmMisprintSignal.setUser(user);
        filmMisprintSignal = errorSignalRepository.save(filmMisprintSignal);
        return filmMisprintSignal;
    }

    public Role getRole(Actor actor, Film film, Long pay) {
        Role role = new Role();
        role.setActor(actor);
        role.setFilm(film);
        role.setAverageMark(9.4);
        role.setPay(pay);
        role.setDescription("bla");
        role = roleRepository.save(role);
        return role;
    }

    public Role getRole(Actor actor, Film film, Double rating) {
        Role role = new Role();
        role.setActor(actor);
        role.setFilm(film);
        role.setAverageMark(rating);
        role.setPay(1000L);
        role.setDescription("bla");
        role = roleRepository.save(role);
        return role;
    }

    public Role getRole(Actor actor, Film film, Long pay, Double rating) {
        Role role = new Role();
        role.setActor(actor);
        role.setFilm(film);
        role.setAverageMark(rating);
        role.setPay(pay);
        role.setDescription("bla");
        role = roleRepository.save(role);
        return role;
    }

    public Role getRole(Actor actor, Film film) {
        Role role = new Role();
        role.setActor(actor);
        role.setFilm(film);
        role.setAverageMark(9.4);
        role.setPay(1000L);
        role.setDescription("bla");
        role = roleRepository.save(role);
        return role;
    }

    public Role getRoleWithoutRating(Actor actor, Film film) {
        Role role = new Role();
        role.setActor(actor);
        role.setFilm(film);
        role.setPay(1000L);
        role.setDescription("bla");
        role = roleRepository.save(role);
        return role;
    }

    public RoleCreateDTO getRoleCreateDTO(UUID actorId, UUID filmId) {
        RoleCreateDTO create = new RoleCreateDTO();
        create.setDescription("bla");
        create.setPay(1000L);
        create.setAverageMark(9.8);
        create.setActorId(actorId);
        create.setFilmId(filmId);
        return create;
    }

    public RolePatchDTO getRolePatch(UUID actorId, UUID filmId) {
        RolePatchDTO patch = new RolePatchDTO();
        patch.setDescription("blaG");
        patch.setPay(100012L);
        patch.setAverageMark(2.2);
        patch.setActorId(actorId);
        patch.setFilmId(filmId);
        return patch;
    }

    public RolePutDTO getRolePut(UUID actorId, UUID filmId) {
        RolePutDTO put = new RolePutDTO();
        put.setDescription("bla222");
        put.setAverageMark(9.8);
        put.setActorId(actorId);
        put.setFilmId(filmId);
        return put;
    }

    public RegisteredUser getRegisteredUser() {
        RegisteredUser registeredUser = new RegisteredUser();
        registeredUser.setName("test");
        registeredUser.setEmail("24");
        registeredUser.setPassword("123");
        registeredUser.setAccessType(UserAccessType.BAN);
        registeredUser.setSex(SexType.FEMALE);
        registeredUser.setTrustLevel(UserTrustLevelType.LOW);
        registeredUser.setPostType(UserPostType.ADMIN);
        registeredUser = userRepository.save(registeredUser);
        return registeredUser;
    }

    public Person getPerson() {
        Person person = new Person();
        person.setName("Name");
        person.setBiography("Live");
        person.setSex(SexType.FEMALE);
        person.setBirthday(LocalDate.parse("1988-05-12"));
        person = personRepository.save(person);
        return person;
    }

    public CrewMember getCrewMember(Person person, Film film) {
        CrewMember crewMember = new CrewMember();
        crewMember.setPerson(person);
        crewMember.setPost(CrewMemberPostType.DIRECTOR);
        crewMember.setFilm(film);
        return crewMember = crewMemberRepository.save(crewMember);
    }

    public void createFilmRating(RegisteredUser user, Film film, Double rating) {
        FilmRating filmRating = new FilmRating();
        filmRating.setFilm(film);
        filmRating.setUser(user);
        filmRating.setRating(rating);
        filmRating = ratingRepository.save(filmRating);
    }

    public void createRoleRating(RegisteredUser user, Role role, Double rating) {
        RoleRating roleRating = new RoleRating();
        roleRating.setRole(role);
        roleRating.setUser(user);
        roleRating.setRating(rating);
        roleRating = ratingRepository.save(roleRating);
    }

    public FilmMisprintSignal createFilmMisPrintSignalChecked(RegisteredUser contentManager, Film film, RegisteredUser user) {
        FilmMisprintSignal filmMisprintSignal = new FilmMisprintSignal();
        filmMisprintSignal.setContentManager(contentManager);
        filmMisprintSignal.setFilm(film);
        filmMisprintSignal.setUser(user);
        filmMisprintSignal.setFixDate(Instant.now());
        filmMisprintSignal.setVerification(VerificationStatusType.CHECKED);
        filmMisprintSignal.setCorrectString("Test");
        filmMisprintSignal.setWrongString("dva");
        filmMisprintSignal = errorSignalRepository.save(filmMisprintSignal);
        return filmMisprintSignal;
    }

    public FilmMisprintSignal createFilmMisPrintSignalNonChecked(Film film, RegisteredUser user) {
        FilmMisprintSignal filmMisprintSignal = new FilmMisprintSignal();
        filmMisprintSignal.setFilm(film);
        filmMisprintSignal.setUser(user);
        filmMisprintSignal.setVerification(VerificationStatusType.NOT_CHECKED);
        filmMisprintSignal.setCorrectString("Test");
        filmMisprintSignal.setWrongString("dva");
        filmMisprintSignal = errorSignalRepository.save(filmMisprintSignal);
        return filmMisprintSignal;
    }

    public FilmMisprintSignalPatchDTO getGetFilmMisprintSignalPatchDTO(Film film) {
        FilmMisprintSignalPatchDTO patch = new FilmMisprintSignalPatchDTO();
        patch.setFilmId(film.getId());
        patch.setVerification(VerificationStatusType.CHECKED);
        patch.setWrongString("dva");
        patch.setCorrectString("Test");
        return patch;
    }

    public FilmMisprintSignalReadDTO toRead(FilmMisprintSignal filmMisprintSignal, RegisteredUser contentManager) {
        FilmMisprintSignalReadDTO read = new FilmMisprintSignalReadDTO();
        read.setId(filmMisprintSignal.getId());
        read.setContentManagerId(contentManager.getId());
        read.setCorrectString(filmMisprintSignal.getCorrectString());
        read.setCreatedAt(filmMisprintSignal.getCreatedAt());
        read.setFilmId(filmMisprintSignal.getFilm().getId());
        read.setFixDate(filmMisprintSignal.getFixDate());
        read.setUpdatedAt(filmMisprintSignal.getUpdatedAt());
        read.setUserId(filmMisprintSignal.getUser().getId());
        read.setWrongString(filmMisprintSignal.getWrongString());
        read.setVerification(filmMisprintSignal.getVerification());
        return read;
    }
}
