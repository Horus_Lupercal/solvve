package homework.backend.example.repository;

import homework.backend.example.domain.persons.Person;
import homework.backend.example.exception.EntityNotFoundException;
import homework.backend.example.repository.person.PersonRepository;
import homework.backend.example.service.TranslationService;
import homework.backend.example.testfactory.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.hibernate.LazyInitializationException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(statements = "delete from person", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class RepositoryHelperTest {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Test
    public void testGetReference() {
        Person p = testObjectsFactory.getPerson();
        Person personReference = repositoryHelper.getReference(Person.class, p.getId());

        Assertions.assertThat(personReference.getId()).isEqualByComparingTo(p.getId());
    }

    @Test
    public void testReference() {
        Person p = testObjectsFactory.getPerson();
        Person personReference = repositoryHelper.getReference(Person.class, p.getId());
        assertThrows(LazyInitializationException.class, personReference::getName);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testReferenceByWrongId() {
        Person person = repositoryHelper.getReference(Person.class, UUID.randomUUID());
    }
}
