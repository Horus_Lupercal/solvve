package homework.backend.example.repository;

import homework.backend.example.domain.persons.Person;
import homework.backend.example.repository.person.PersonRepository;
import homework.backend.example.testfactory.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(statements = "delete from person", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class PersonRepositoryTest {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private TestObjectsFactory objectsFactory;

    @Test
    public void testSave() {
        Person person = new Person();
        person = personRepository.save(person);
        assertNotNull(person.getId());
        assertTrue(personRepository.findById(person.getId()).isPresent());
    }

    @Test
    public void testCreatedAtIsSet() {
        Person person = objectsFactory.getPerson();

        Instant createdAtBeforeReload = person.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);

        person = personRepository.findById(person.getId()).get();

        Instant createdAtAfterReload = person.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testModifiedAtIsSet() {
        Person person = objectsFactory.getPerson();
        Instant updatedAtBeforeReload = person.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);

        person = personRepository.findById(person.getId()).get();
        Instant updatedAtAfterReload = person.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        person.setName("New Name");
        person = personRepository.save(person);

        person = personRepository.findById(person.getId()).get();
        Instant updatedAtAfterChanges = person.getUpdatedAt();

        Assert.assertNotEquals(updatedAtAfterReload, updatedAtAfterChanges);
        Assert.assertTrue(updatedAtAfterChanges.getNano() > updatedAtAfterReload.getNano());
    }
}

