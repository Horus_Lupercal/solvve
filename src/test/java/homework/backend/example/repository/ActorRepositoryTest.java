package homework.backend.example.repository;

import homework.backend.example.domain.persons.Actor;
import homework.backend.example.domain.persons.Person;
import homework.backend.example.repository.actor.ActorRepository;
import homework.backend.example.repository.person.PersonRepository;
import homework.backend.example.testfactory.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(statements = {"delete from actor",
                   "delete from person"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class ActorRepositoryTest {

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private TestObjectsFactory objectsFactory;

    @Test
    public void testSave() {
        Person person = new Person();
        person = personRepository.save(person);
        Actor actor = new Actor();
        actor.setPerson(person);
        actor = actorRepository.save(actor);
        assertNotNull(actor.getId());
        assertTrue(actorRepository.findById(actor.getId()).isPresent());
    }

    @Test
    public void testCreatedAtIsSet() throws Exception {
        Person person = objectsFactory.getPerson();
        Actor actor = objectsFactory.getActor(person);

        Instant createdAtBeforeReload = actor.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);

        actor = actorRepository.findById(actor.getId()).get();

        Instant createdAtAfterReload = actor.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testModifiedAtIsSet() throws Exception {
       Person person = new Person();
       objectsFactory.setFields(person);
       Actor actor = objectsFactory.getActor(person);

       Instant createdAtBeforeReload = actor.getUpdatedAt();
       Assert.assertNotNull(createdAtBeforeReload);

       actor = actorRepository.findById(actor.getId()).get();

       Instant createdAtAfterReload = actor.getUpdatedAt();
       Assert.assertNotNull(createdAtAfterReload);

       actor.setMiddlePay(1200L);
       actor = actorRepository.save(actor);

       actor = actorRepository.findById(actor.getId()).get();
       Instant createdAtAfterChanges = actor.getUpdatedAt();
       Assert.assertNotEquals(createdAtAfterReload, createdAtAfterChanges);
    }
}
