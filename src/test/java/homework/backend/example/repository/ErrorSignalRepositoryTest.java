package homework.backend.example.repository;

import homework.backend.example.domain.content.Film;
import homework.backend.example.domain.content.Role;
import homework.backend.example.domain.enums.VerificationStatusType;
import homework.backend.example.domain.massages.errormassages.errorsignal.ReviewErrorSignal;
import homework.backend.example.domain.massages.errormassages.misprintsignal.FilmMisprintSignal;
import homework.backend.example.domain.persons.Actor;
import homework.backend.example.domain.persons.Person;
import homework.backend.example.domain.users.RegisteredUser;
import homework.backend.example.repository.errorsignal.ErrorSignalRepository;
import homework.backend.example.testfactory.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(statements = { "delete from error_signal",
                    "delete from registered_user",
                    "delete from film"},
                     executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class ErrorSignalRepositoryTest {

    @Autowired
    private ErrorSignalRepository errorSignalRepository;

    @Autowired
    private TestObjectsFactory objectsFactory;

    @Test
    public void testSave() {
        ReviewErrorSignal errorSignal = new ReviewErrorSignal();
        errorSignal = errorSignalRepository.save(errorSignal);
        Assert.assertNotNull(errorSignal.getId());
        Assert.assertTrue(errorSignalRepository.findById(errorSignal.getId()).isPresent());
    }

    @Test
    public void testFindByVerification() throws Exception {
        RegisteredUser user = new RegisteredUser();
        objectsFactory.setFields(user);
        Film film1 = objectsFactory.getFilm();
        Film film2 = objectsFactory.getFilm();

        FilmMisprintSignal filmMisprintSignal1 = objectsFactory.getFilmMisprintSignal(
                film1, user, VerificationStatusType.NOT_CHECKED);
        objectsFactory.getFilmMisprintSignal(film2, user, VerificationStatusType.CHECKED);
        FilmMisprintSignal filmMisprintSignal2 = objectsFactory.getFilmMisprintSignal(
                film2, user, VerificationStatusType.NOT_CHECKED);
        objectsFactory.getFilmMisprintSignal(film2, user, VerificationStatusType.CHECKED);

        List<FilmMisprintSignal> res = errorSignalRepository.findByVerification(VerificationStatusType.NOT_CHECKED);
        Assertions.assertThat(res).extracting(FilmMisprintSignal::getId)
                .containsExactlyInAnyOrder(filmMisprintSignal1.getId(), filmMisprintSignal2.getId());
    }

    @Test
    public void testFindByWrongString() throws Exception {
        RegisteredUser user = new RegisteredUser();
        objectsFactory.setFields(user);
        Film film1 = objectsFactory.getFilm();
        Film film2 = objectsFactory.getFilm();

        FilmMisprintSignal filmMisprintSignal1 = objectsFactory.getFilmMisprintSignal(
                film1, user, "Wrong");
        objectsFactory.getFilmMisprintSignal(film2, user, "123");
        FilmMisprintSignal filmMisprintSignal2 = objectsFactory.getFilmMisprintSignal(
                film2, user, "Wrong");
        objectsFactory.getFilmMisprintSignal(film2, user, "325");

        List<FilmMisprintSignal> res = errorSignalRepository.findByWrongString("Wrong");
        Assertions.assertThat(res).extracting(FilmMisprintSignal::getId)
                .containsExactlyInAnyOrder(filmMisprintSignal1.getId(), filmMisprintSignal2.getId());
    }

    @Test
    public void testFindByVerificationAndWrongString() throws Exception {
        RegisteredUser user = new RegisteredUser();
        objectsFactory.setFields(user);
        Film film1 = objectsFactory.getFilm();
        Film film2 = objectsFactory.getFilm();

        FilmMisprintSignal filmMisprintSignal1 = objectsFactory.getFilmMisprintSignal(
                film1, user, "Wrong", VerificationStatusType.NOT_CHECKED);

        objectsFactory.getFilmMisprintSignal(film2, user, "123", VerificationStatusType.NOT_CHECKED);

        FilmMisprintSignal filmMisprintSignal2 = objectsFactory.getFilmMisprintSignal(
                film2, user, "Wrong", VerificationStatusType.NOT_CHECKED);

        objectsFactory.getFilmMisprintSignal(film2, user, "Wrong", VerificationStatusType.CHECKED);

        List<FilmMisprintSignal> res = errorSignalRepository.findByVerificationAndWrongString(
                VerificationStatusType.NOT_CHECKED, "Wrong");
        Assertions.assertThat(res).extracting(FilmMisprintSignal::getId)
                .containsExactlyInAnyOrder(filmMisprintSignal1.getId(), filmMisprintSignal2.getId());
    }

    @Test
    public void testFindByIdAndWrongString() throws Exception {
        RegisteredUser user = new RegisteredUser();
        objectsFactory.setFields(user);
        Film film1 = objectsFactory.getFilm();
        Film film2 = objectsFactory.getFilm();

        FilmMisprintSignal filmMisprintSignal1 = objectsFactory.getFilmMisprintSignal(
                film1, user, "Wrong");

        objectsFactory.getFilmMisprintSignal(film1, user, "123");

        FilmMisprintSignal filmMisprintSignal2 = objectsFactory.getFilmMisprintSignal(
                film1, user, "Wrong");

        objectsFactory.getFilmMisprintSignal(film2, user, "Wrong");

        List<FilmMisprintSignal> res = errorSignalRepository.findByFilmIdAndWrongString(
                film1.getId(), "Wrong");
        Assertions.assertThat(res).extracting(FilmMisprintSignal::getId)
                .containsExactlyInAnyOrder(filmMisprintSignal1.getId(), filmMisprintSignal2.getId());
    }
}
