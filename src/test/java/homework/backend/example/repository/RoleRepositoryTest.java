package homework.backend.example.repository;

import homework.backend.example.domain.content.Film;
import homework.backend.example.domain.content.Role;
import homework.backend.example.domain.persons.Actor;
import homework.backend.example.domain.persons.Person;
import homework.backend.example.domain.users.RegisteredUser;
import homework.backend.example.repository.role.RoleRepository;
import homework.backend.example.testfactory.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(statements = {"delete from role",
                   "delete from film",
                   "delete from actor",
                   "delete from person"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class RoleRepositoryTest {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testSave() {
        Role role = new Role();
        role = roleRepository.save(role);
        assertNotNull(role.getId());
        assertTrue(roleRepository.findById(role.getId()).isPresent());
    }

    @Test
    public void testCreatedAtIsSet() throws Exception {
        Person person = testObjectsFactory.getPerson();
        Actor actor = testObjectsFactory.getActor(person);
        Film film = testObjectsFactory.getFilm();

        Role role = testObjectsFactory.getRole(actor, film);

        Instant createdAtBeforeReload = role.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);

        role = roleRepository.findById(role.getId()).get();

        Instant createdAtAfterReload = role.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testModifiedAtIsSet() throws Exception {
        Person person = testObjectsFactory.getPerson();
        Actor actor = testObjectsFactory.getActor(person);
        Film film = testObjectsFactory.getFilm();

        Role role = testObjectsFactory.getRole(actor, film);
        Instant updatedAtBeforeReload = role.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);

        role = roleRepository.findById(role.getId()).get();
        Instant updatedAtAfterReload = role.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        role.setDescription("New Description");
        role = roleRepository.save(role);

        role = roleRepository.findById(role.getId()).get();
        Instant updatedAtAfterChanges = role.getUpdatedAt();

        Assert.assertNotEquals(updatedAtAfterReload, updatedAtAfterChanges);
        Assert.assertTrue(updatedAtAfterChanges.getNano() > updatedAtAfterReload.getNano());
    }

    @Test
    public void testGetActorPay() throws Exception {
        Person person = testObjectsFactory.getPerson();
        Actor actor1 = testObjectsFactory.getActor(person);
        Film film1 = testObjectsFactory.getFilm();
        Film film2 = testObjectsFactory.getFilm();

        Role role1 = testObjectsFactory.getRole(actor1, film1, 1000L);
        testObjectsFactory.getRole(actor1, film2, 200L);
        Role role2 = testObjectsFactory.getRole(actor1, film2, 1000L);
        testObjectsFactory.getRole(actor1, film2, 900L);

        List<Role> res = roleRepository.findByActorIdAndPay(actor1.getId(), 1000L);
        Assertions.assertThat(res).extracting(Role::getId).containsExactlyInAnyOrder(role1.getId(), role2.getId());
    }

    @Test
    public void testGetActorPayGreaterThan() throws Exception {
        Person person = testObjectsFactory.getPerson();
        Actor actor = testObjectsFactory.getActor(person);
        Film film1 = testObjectsFactory.getFilm();
        Film film2 = testObjectsFactory.getFilm();
        Role role1 = testObjectsFactory.getRole(actor, film1, 1000L);
        testObjectsFactory.getRole(actor, film1, 500L);
        Role role2 = testObjectsFactory.getRole(actor, film2, 2000L);
        testObjectsFactory.getRole(actor, film1, 400L);
        testObjectsFactory.getRole(actor, film2, 700L);

        List<Role> res = roleRepository.findByActorIdByPayGreaterThan(actor.getId(), 900L);
        Assertions.assertThat(res).extracting(Role::getId).containsExactlyInAnyOrder(role1.getId(), role2.getId());
    }

    @Test
    public void testGetIdsOfRoles() throws Exception {
        Person person = testObjectsFactory.getPerson();
        Actor actor = testObjectsFactory.getActor(person);
        Film film = testObjectsFactory.getFilm();

        Set<UUID> expectedIdsOfRoles = new HashSet<>();
        expectedIdsOfRoles.add(testObjectsFactory.getRoleWithoutRating(actor, film).getId());
        expectedIdsOfRoles.add(testObjectsFactory.getRoleWithoutRating(actor, film).getId());

        transactionTemplate.executeWithoutResult(status -> {
            Assert.assertEquals(expectedIdsOfRoles, roleRepository.getIdsOfRoles().collect(Collectors.toSet()));
        });
    }
}
