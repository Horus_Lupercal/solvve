package homework.backend.example.repository;

import homework.backend.example.domain.enums.CrewMemberPostType;
import homework.backend.example.domain.persons.CrewMember;
import homework.backend.example.repository.crewmember.CrewMemberRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(statements = "delete from crew_member",
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class CrewMemberRepositoryTest {

    @Autowired
    private CrewMemberRepository crewMemberRepository;

    @Test
    public void testSave() {
        CrewMember c = new CrewMember();
        c = crewMemberRepository.save(c);
        assertNotNull(c.getId());
        assertTrue(crewMemberRepository.findById(c.getId()).isPresent());
    }

    @Test
    public void testCreatedAtIsSet() {
        CrewMember c = new CrewMember();
        c = crewMemberRepository.save(c);

        Instant createdAtBeforeReload = c.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);

        c = crewMemberRepository.findById(c.getId()).get();

        Instant createdAtAfterReload = c.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testModifiedAtIsSet() {
        CrewMember c = new CrewMember();
        c = crewMemberRepository.save(c);

        Instant updatedAtBeforeReload = c.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);

        c = crewMemberRepository.findById(c.getId()).get();
        Instant updatedAtAfterReload = c.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        c.setPost(CrewMemberPostType.DIRECTOR);
        c = crewMemberRepository.save(c);

        c = crewMemberRepository.findById(c.getId()).get();
        Instant updatedAtAfterChanges = c.getUpdatedAt();

        Assert.assertNotEquals(updatedAtAfterReload, updatedAtAfterChanges);
        Assert.assertTrue(updatedAtAfterChanges.getNano() > updatedAtAfterReload.getNano());
    }
}
