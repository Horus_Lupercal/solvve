package homework.backend.example.repository;

import homework.backend.example.domain.content.Film;
import homework.backend.example.repository.film.FilmRepository;
import homework.backend.example.testfactory.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(statements = "delete from film", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class FilmRepositoryTest {

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private TestObjectsFactory objectsFactory;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testSave() throws Exception {
        Film f = new Film();
        objectsFactory.setFields(f);
        assertNotNull(f.getId());
        assertTrue(filmRepository.findById(f.getId()).isPresent());
    }

    @Test
    public void testCreatedAtIsSet() throws Exception {
        Film film = new Film();
        objectsFactory.setFields(film);
        Instant createdAtBeforeReload = film.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);

        film = filmRepository.findById(film.getId()).get();
        Instant createdAtAfterReload = film.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testModifiedAtIsSet() throws Exception {
        Film film = new Film();
        objectsFactory.setFields(film);
        Instant updatedAtBeforeReload = film.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);

        film = filmRepository.findById(film.getId()).get();
        Instant updatedAtAfterReload = film.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        film.setTitle("New Title");
        film = filmRepository.save(film);

        film = filmRepository.findById(film.getId()).get();
        Instant updatedAtAfterChanges = film.getUpdatedAt();

        Assert.assertNotEquals(updatedAtAfterReload, updatedAtAfterChanges);
        Assert.assertTrue(updatedAtAfterChanges.getNano() > updatedAtAfterReload.getNano());
    }

    @Test
    public void testGetIdOfFilms() throws Exception {
        Film film1 = new Film();
        objectsFactory.setFields(film1);
        Film film2 = new Film();
        objectsFactory.setFields(film2);

        Set<UUID> expectedIdsOfFilms = new HashSet<>();
        expectedIdsOfFilms.add(film1.getId());
        expectedIdsOfFilms.add(film2.getId());

        transactionTemplate.executeWithoutResult(status -> {
            Assert.assertEquals(expectedIdsOfFilms, filmRepository.getIdsOfFilms().collect(Collectors.toSet()));
        });
    }
}
