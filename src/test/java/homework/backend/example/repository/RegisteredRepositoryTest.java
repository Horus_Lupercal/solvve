package homework.backend.example.repository;

import homework.backend.example.domain.users.RegisteredUser;
import homework.backend.example.repository.registereduser.RegisteredUserRepository;
import homework.backend.example.testfactory.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(statements = "delete from registered_user", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class RegisteredRepositoryTest {

    @Autowired
    private RegisteredUserRepository userRepository;

    @Autowired
    private TestObjectsFactory objectsFactory;

    @Test
    public void testSave() throws Exception {
        RegisteredUser user = new RegisteredUser();
        objectsFactory.setFields(user);
        assertNotNull(user.getId());
        assertTrue(userRepository.findById(user.getId()).isPresent());
    }

    @Test
    public void testCreatedAtIsSet() {
        RegisteredUser user = objectsFactory.getRegisteredUser();

        Instant createdAtBeforeReload = user.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);

        user = userRepository.findById(user.getId()).get();

        Instant createdAtAfterReload = user.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testModifiedAtIsSet() {
        RegisteredUser user = objectsFactory.getRegisteredUser();
        Instant updatedAtBeforeReload = user.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);

        user = userRepository.findById(user.getId()).get();
        Instant updatedAtAfterReload = user.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        user.setName("New Name");
        user = userRepository.save(user);

        user = userRepository.findById(user.getId()).get();
        Instant modifiedAfterChanges = user.getUpdatedAt();

        Assert.assertNotEquals(updatedAtAfterReload, modifiedAfterChanges);
        Assert.assertTrue(modifiedAfterChanges.getNano() > updatedAtAfterReload.getNano());
    }
}
