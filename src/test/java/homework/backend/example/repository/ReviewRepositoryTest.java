package homework.backend.example.repository;

import homework.backend.example.domain.massages.review.FilmReview;
import homework.backend.example.domain.massages.review.Review;
import homework.backend.example.domain.massages.review.RoleReview;
import homework.backend.example.repository.review.ReviewRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(statements = "delete from review", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class ReviewRepositoryTest {

    @Autowired
    private ReviewRepository reviewRepository;

    @Test
    public void saveReview() {
        FilmReview fr = new FilmReview();
        fr = reviewRepository.save(fr);
        assertNotNull(fr.getId());
        assertTrue(reviewRepository.findById(fr.getId()).isPresent());

        RoleReview rr = new RoleReview();
        rr = reviewRepository.save(rr);
        assertNotNull(rr.getId());
        assertTrue(reviewRepository.findById(rr.getId()).isPresent());
    }
}
