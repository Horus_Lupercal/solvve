package homework.backend.example.repository;

import homework.backend.example.domain.content.Film;
import homework.backend.example.domain.content.Role;
import homework.backend.example.domain.ententies.rating.filmrating.FilmRating;
import homework.backend.example.domain.ententies.rating.rolerating.RoleRating;
import homework.backend.example.domain.users.RegisteredUser;
import homework.backend.example.repository.rating.RatingRepository;
import homework.backend.example.testfactory.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@Sql(statements = {"delete from rating",
        "delete from film",
        "delete from registered_user"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class RatingRepositoryTest {

    @Autowired
    private RatingRepository ratingRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testFilmRatingSave() throws Exception {
        FilmRating filmRating = new FilmRating();
        filmRating = ratingRepository.save(filmRating);
        assertNotNull(filmRating.getId());
        assertTrue(ratingRepository.findById(filmRating.getId()).isPresent());
    }

    @Test
    public void testFilmRatingCreatedAtIsSet() throws Exception {
        FilmRating filmRating = new FilmRating();
        filmRating = ratingRepository.save(filmRating);
        Instant createdAtBeforeReload = filmRating.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);

        filmRating = (FilmRating) ratingRepository.findById(filmRating.getId()).get();
        Instant createdAtAfterReload = filmRating.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testFilmRatingModifiedAtIsSet() throws Exception {
        FilmRating filmRating = new FilmRating();
        filmRating = ratingRepository.save(filmRating);
        Instant updatedAtBeforeReload = filmRating.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);

        filmRating = (FilmRating) ratingRepository.findById(filmRating.getId()).get();
        Instant updatedAtAfterReload = filmRating.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        Film film = new Film();
        testObjectsFactory.setFields(film);

        filmRating.setFilm(film);
        filmRating = ratingRepository.save(filmRating);

        filmRating = (FilmRating) ratingRepository.findById(filmRating.getId()).get();
        Instant updatedAtAfterChanges = filmRating.getUpdatedAt();

        Assert.assertNotEquals(updatedAtAfterReload, updatedAtAfterChanges);
        Assert.assertTrue(updatedAtAfterChanges.getNano() > updatedAtAfterReload.getNano());
    }

    @Test
    public void testRoleRatingSave() throws Exception {
        RoleRating roleRating = new RoleRating();
        roleRating = ratingRepository.save(roleRating);
        assertNotNull(roleRating.getId());
        assertTrue(ratingRepository.findById(roleRating.getId()).isPresent());
    }

    @Test
    public void testRoleRatingCreatedAtIsSet() throws Exception {
        RoleRating roleRating = new RoleRating();
        roleRating = ratingRepository.save(roleRating);
        Instant createdAtBeforeReload = roleRating.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);

        roleRating = (RoleRating) ratingRepository.findById(roleRating.getId()).get();
        Instant createdAtAfterReload = roleRating.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testRoleRatingModifiedAtIsSet() throws Exception {
        RoleRating roleRating = new RoleRating();
        roleRating = ratingRepository.save(roleRating);
        Instant updatedAtBeforeReload = roleRating.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);

        roleRating = (RoleRating) ratingRepository.findById(roleRating.getId()).get();
        Instant updatedAtAfterReload = roleRating.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertEquals(updatedAtBeforeReload, updatedAtAfterReload);

        Role role = new Role();
        testObjectsFactory.setFields(role);

        roleRating.setRole(role);
        roleRating = ratingRepository.save(roleRating);

        roleRating = (RoleRating) ratingRepository.findById(roleRating.getId()).get();
        Instant updatedAtAfterChanges = roleRating.getUpdatedAt();

        Assert.assertNotEquals(updatedAtAfterReload, updatedAtAfterChanges);
        Assert.assertTrue((updatedAtAfterChanges.getNano() - updatedAtAfterReload.getNano()) > 0);
    }


    @Test
    public void testCalcFilmAverageMark() throws Exception {
        RegisteredUser user = new RegisteredUser();
        testObjectsFactory.setFields(user);
        Film film1 = new Film();
        testObjectsFactory.setFields(film1);
        Film film2 = new Film();
        testObjectsFactory.setFields(film2);

        testObjectsFactory.createFilmRating(user, film1, 4.0);
        testObjectsFactory.createFilmRating(user, film1, 5.0);
        testObjectsFactory.createFilmRating(user, film2, 3.0);
        testObjectsFactory.createFilmRating(user, film1, null);

        Assert.assertEquals(4.5, ratingRepository.calcAverageMarkOfFilm(film1.getId()), Double.MIN_NORMAL);
    }

    @Test
    public void testCalcRoleAverageMark() throws Exception {
        RegisteredUser user = new RegisteredUser();
        testObjectsFactory.setFields(user);
        Role role1 = new Role();
        testObjectsFactory.setFields(role1);
        Role role2 = new Role();
        testObjectsFactory.setFields(role2);

        testObjectsFactory.createRoleRating(user, role1, 4.0);
        testObjectsFactory.createRoleRating(user, role1, 5.0);
        testObjectsFactory.createRoleRating(user, role2, 3.0);
        testObjectsFactory.createRoleRating(user, role1, null);

        Assert.assertEquals(4.5, ratingRepository.calcAverageMarkOfRole(role1.getId()), Double.MIN_NORMAL);
    }
}
