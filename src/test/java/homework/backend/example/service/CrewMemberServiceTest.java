package homework.backend.example.service;

import homework.backend.example.domain.content.Film;
import homework.backend.example.domain.persons.CrewMember;
import homework.backend.example.domain.persons.Person;
import homework.backend.example.dto.crewmember.CrewMemberCreateDTO;
import homework.backend.example.dto.crewmember.CrewMemberPatchDTO;
import homework.backend.example.dto.crewmember.CrewMemberPutDTO;
import homework.backend.example.dto.crewmember.CrewMemberReadDTO;
import homework.backend.example.exception.EntityNotFoundException;
import homework.backend.example.repository.crewmember.CrewMemberRepository;
import homework.backend.example.repository.film.FilmRepository;
import homework.backend.example.testfactory.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Sql(statements = "delete from crew_member", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class CrewMemberServiceTest {
    @Autowired
    private CrewMemberRepository crewMemberRepository;

    @Autowired
    private CrewMemberService crewMemberService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private FilmRepository filmRepository;

    @Test
    public void testGetCrewMember() throws Exception {
        Person person = new Person();
        testObjectsFactory.setFields(person);
        Film film = new Film();
        testObjectsFactory.setFields(film);
        CrewMember crewMember = testObjectsFactory.getCrewMember(person, film);

        CrewMemberReadDTO read = crewMemberService.getCrewMember(crewMember.getId());

        Assertions.assertThat(crewMember).isEqualToIgnoringGivenFields(read, "film", "person");
        Assertions.assertThat(crewMember.getPerson()).isEqualToComparingOnlyGivenFields(person);
        Assertions.assertThat(crewMember.getFilm()).isEqualToIgnoringGivenFields(film);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetCrewMemberByWrongId() {
        crewMemberService.getCrewMember(UUID.randomUUID());
    }

    @Test
    public void testCreateCrewMember() throws Exception {
        Person person = new Person();
        testObjectsFactory.setFields(person);
        Film film = new Film();
        testObjectsFactory.setFields(film);

        CrewMemberCreateDTO create = new CrewMemberCreateDTO();
        testObjectsFactory.setFields(create);
        create.setPersonId(person.getId());
        create.setFilmId(film.getId());

        CrewMemberReadDTO read = crewMemberService.createCrewMember(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        CrewMember crewMember = crewMemberRepository.findById(read.getId()).get();

        Assertions.assertThat(crewMember).isEqualToIgnoringGivenFields(read, "film", "person");
        Assertions.assertThat(crewMember.getPerson()).isEqualToComparingOnlyGivenFields(person);
        Assertions.assertThat(crewMember.getFilm()).isEqualToIgnoringGivenFields(film);
    }

    @Test
    public void testPatchCrewMember() throws Exception {
        Person person = new Person();
        testObjectsFactory.setFields(person);
        Film film = new Film();
        testObjectsFactory.setFields(film);
        CrewMember crewMember = testObjectsFactory.getCrewMember(person, film);

        CrewMemberPatchDTO patch = new CrewMemberPatchDTO();
        testObjectsFactory.setFields(patch);

        CrewMemberReadDTO read = crewMemberService.patchCrewMember(crewMember.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        crewMember = crewMemberRepository.findById(read.getId()).get();

        Assertions.assertThat(crewMember).isEqualToIgnoringGivenFields(read, "film", "person");
        Assertions.assertThat(crewMember.getPerson()).isEqualToComparingOnlyGivenFields(person);
        Assertions.assertThat(crewMember.getFilm()).isEqualToIgnoringGivenFields(film);
    }

    @Test
    public void testPatchCrewMemberEmptyPatch() throws Exception {
        Person person = new Person();
        testObjectsFactory.setFields(person);
        Film film = new Film();
        testObjectsFactory.setFields(film);
        CrewMember crewMember = testObjectsFactory.getCrewMember(person, film);

        CrewMemberPatchDTO patch = new CrewMemberPatchDTO();

        CrewMemberReadDTO read = crewMemberService.patchCrewMember(crewMember.getId(), patch);
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        CrewMember crewMemberAfterUpdate = crewMemberRepository.findById(read.getId()).get();

        Assertions.assertThat(crewMemberAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(crewMember).isEqualToComparingFieldByField(crewMemberAfterUpdate);
    }

    @Test
    public void testPutActor() throws Exception {
        Person person = testObjectsFactory.getPerson();
        Film film = testObjectsFactory.getFilm();
        CrewMember crewMember = testObjectsFactory.getCrewMember(person, film);

        CrewMemberPutDTO put = new CrewMemberPutDTO();
        CrewMemberReadDTO read = crewMemberService.updateCrewMember(crewMember.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        crewMember = crewMemberRepository.findById(read.getId()).get();

        Assertions.assertThat(crewMember).isEqualToIgnoringGivenFields(read, "film", "person");
        Assertions.assertThat(crewMember.getPerson()).isEqualToComparingOnlyGivenFields(person);
        Assertions.assertThat(crewMember.getFilm()).isEqualToIgnoringGivenFields(film);
    }

    @Test
    public void testDeleteNews() throws Exception {
        Person person = new Person();
        testObjectsFactory.setFields(person);
        Film film = new Film();
        testObjectsFactory.setFields(film);
        CrewMember crewMember = new CrewMember();
        testObjectsFactory.setFields(crewMember);
        crewMember.setPerson(person);
        crewMember.setFilm(film);

        crewMemberService.deleteCrewMember(crewMember.getId());
        Assert.assertFalse(crewMemberRepository.existsById(crewMember.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteNewsNotFound() {
        crewMemberService.deleteCrewMember(UUID.randomUUID());
    }
}
