package homework.backend.example.service;

import homework.backend.example.domain.users.RegisteredUser;
import homework.backend.example.dto.registreduser.RegisteredUserCreateDTO;
import homework.backend.example.dto.registreduser.RegisteredUserPatchDTO;
import homework.backend.example.dto.registreduser.RegisteredUserPutDTO;
import homework.backend.example.dto.registreduser.RegisteredUserReadDTO;
import homework.backend.example.exception.EntityNotFoundException;
import homework.backend.example.repository.registereduser.RegisteredUserRepository;
import homework.backend.example.testfactory.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Sql(statements = "delete from registered_user", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class RegisteredUserTest {

    @Autowired
    private RegisteredUserService userService;

    @Autowired
    private RegisteredUserRepository userRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetUser() throws Exception {
        RegisteredUser user = new RegisteredUser();
        testObjectsFactory.setFields(user);
        System.out.println(user.getName());

        RegisteredUserReadDTO read = userService.getUser(user.getId());

        Assertions.assertThat(user).isEqualToIgnoringGivenFields(read, "likes", "reviews", "checks");
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetUserByWrongId() {
        userService.getUser(UUID.randomUUID());
    }

    @Test
    public void testCreateUser() throws Exception {
        RegisteredUserCreateDTO create = new RegisteredUserCreateDTO();
        testObjectsFactory.setFields(create);

        RegisteredUserReadDTO read = userService.createUser(create);

        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        RegisteredUser user = userRepository.findById(read.getId()).get();

        Assertions.assertThat(read).isEqualToComparingFieldByField(user);
    }

    @Test
    public void testPatchUser() throws Exception {
        RegisteredUser user = new RegisteredUser();
        testObjectsFactory.setFields(user);

        RegisteredUserPatchDTO patch = new RegisteredUserPatchDTO();
        testObjectsFactory.setFields(patch);

        RegisteredUserReadDTO read = userService.patchUser(user.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        user = userRepository.findById(read.getId()).get();
        Assertions.assertThat(user).isEqualToIgnoringGivenFields(read, "likes", "reviews", "checks");
    }

    @Test
    public void testPatchPersonEmptyPatch() throws Exception {
        RegisteredUser user = new RegisteredUser();
        testObjectsFactory.setFields(user);

        RegisteredUserPatchDTO patch = new RegisteredUserPatchDTO();

        RegisteredUserReadDTO read = userService.patchUser(user.getId(), patch);

        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        RegisteredUser userAfterUpdate = userRepository.findById(read.getId()).get();

        Assertions.assertThat(userAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(user).isEqualToIgnoringGivenFields(read, "likes", "reviews", "checks");
    }

    @Test
    public void testPutPerson() throws Exception {
        RegisteredUser user = new RegisteredUser();
        testObjectsFactory.setFields(user);

        RegisteredUserPutDTO put = new RegisteredUserPutDTO();

        RegisteredUserReadDTO read = userService.upDateUser(user.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        user = userRepository.findById(read.getId()).get();
        Assertions.assertThat(user).isEqualToIgnoringGivenFields(read, "likes", "reviews", "checks");
    }

    @Test
    public void testDeletePerson() {
        RegisteredUser user = testObjectsFactory.getRegisteredUser();

        userService.deleteUser(user.getId());
        Assert.assertFalse(userRepository.existsById(user.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeletePersonNotFound() {
        userService.deleteUser(UUID.randomUUID());
    }
}
