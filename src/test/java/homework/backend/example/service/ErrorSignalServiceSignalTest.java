package homework.backend.example.service;

import homework.backend.example.domain.content.Film;
import homework.backend.example.domain.enums.VerificationStatusType;
import homework.backend.example.domain.massages.errormassages.misprintsignal.FilmMisprintSignal;
import homework.backend.example.domain.users.RegisteredUser;
import homework.backend.example.dto.misprintsignal.FilmMisprintSignalPatchDTO;
import homework.backend.example.dto.misprintsignal.FilmMisprintSignalPutDTO;
import homework.backend.example.dto.misprintsignal.FilmMisprintSignalReadDTO;
import homework.backend.example.exception.EntityNotFoundException;
import homework.backend.example.exception.EntityStatusException;
import homework.backend.example.repository.errorsignal.ErrorSignalRepository;
import homework.backend.example.testfactory.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Sql(statements = { "delete from error_signal",
                    "delete from film",
                    "delete from registered_user" }, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class ErrorSignalServiceSignalTest {

    @Autowired
    private ErrorSignalRepository errorSignalRepository;

    @Autowired
    private FilmMisprintSignalService filmMisprintSignalService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetFilmMisprintSignal() throws Exception {
        Film film = new Film();
        testObjectsFactory.setFields(film);
        RegisteredUser user = new RegisteredUser();
        testObjectsFactory.setFields(user);
        RegisteredUser contentManager = new RegisteredUser();
        testObjectsFactory.setFields(contentManager);

        FilmMisprintSignal filmMisprintSignal = testObjectsFactory.createFilmMisPrintSignalChecked(contentManager, film, user);

        filmMisprintSignal = (FilmMisprintSignal) errorSignalRepository.findById(filmMisprintSignal.getId()).get();

        FilmMisprintSignalReadDTO read = filmMisprintSignalService.getFilmMisprintSignal(filmMisprintSignal.getId());

        Assertions.assertThat(filmMisprintSignal)
                .isEqualToIgnoringGivenFields(read, "film", "user", "contentManager");
        Assertions.assertThat(filmMisprintSignal.getFilm()).isEqualToIgnoringGivenFields(film);
        Assertions.assertThat(filmMisprintSignal.getContentManager())
                .isEqualToIgnoringGivenFields(contentManager, "likes", "reviews", "checks");
        Assertions.assertThat(filmMisprintSignal.getUser())
                .isEqualToIgnoringGivenFields(user, "likes", "reviews", "checks");
    }

    @Test
    public void testEmptyPatchFilmMisprintSignal() throws Exception {
        Film film = new Film();
        testObjectsFactory.setFields(film);
        RegisteredUser user = new RegisteredUser();
        testObjectsFactory.setFields(user);
        RegisteredUser contentManager = new RegisteredUser();
        testObjectsFactory.setFields(contentManager);

        FilmMisprintSignal filmMisprintSignal = testObjectsFactory.createFilmMisPrintSignalChecked(contentManager, film, user);

        filmMisprintSignal = (FilmMisprintSignal) errorSignalRepository.findById(filmMisprintSignal.getId()).get();

        FilmMisprintSignalPatchDTO patch = new FilmMisprintSignalPatchDTO();

        FilmMisprintSignalReadDTO read = filmMisprintSignalService
                .patchFilmMisprintSignal(filmMisprintSignal.getId(), patch);

        Assertions.assertThat(filmMisprintSignal).hasNoNullFieldsOrProperties();
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        FilmMisprintSignal misprintSignalAfterUpdate =
                (FilmMisprintSignal) errorSignalRepository.findById(filmMisprintSignal.getId()).get();

        Assertions.assertThat(misprintSignalAfterUpdate).hasNoNullFieldsOrProperties();
        Assertions.assertThat(filmMisprintSignal)
                .isEqualToIgnoringGivenFields(
                        misprintSignalAfterUpdate, "film", "user", "contentManager");
    }

    @Test
    public void testPathFilmMisprintSignal() throws Exception {
        Film film = new Film();
        testObjectsFactory.setFields(film);
        RegisteredUser user = new RegisteredUser();
        testObjectsFactory.setFields(user);
        RegisteredUser contentManager = new RegisteredUser();
        testObjectsFactory.setFields(contentManager);

        FilmMisprintSignal filmMisprintSignal = testObjectsFactory.createFilmMisPrintSignalChecked(contentManager, film, user);

        filmMisprintSignal = (FilmMisprintSignal) errorSignalRepository.findById(filmMisprintSignal.getId()).get();

        FilmMisprintSignalPatchDTO patch = testObjectsFactory.getGetFilmMisprintSignalPatchDTO(film);

        FilmMisprintSignalReadDTO read = filmMisprintSignalService
                .patchFilmMisprintSignal(filmMisprintSignal.getId(), patch);

        Assertions.assertThat(patch)
                .isEqualToIgnoringGivenFields(read, "film", "user", "contentManager");

        Assertions.assertThat(filmMisprintSignal.getFilm()).isEqualToIgnoringGivenFields(film);
        Assertions.assertThat(filmMisprintSignal.getContentManager())
                .isEqualToIgnoringGivenFields(contentManager, "likes", "reviews", "checks");
        Assertions.assertThat(filmMisprintSignal.getUser())
                .isEqualToIgnoringGivenFields(user, "likes", "reviews", "checks");
    }

    @Test
    public void DeleteFilmMisprintSignal() throws Exception {
        Film film = new Film();
        testObjectsFactory.setFields(film);
        RegisteredUser user = new RegisteredUser();
        testObjectsFactory.setFields(user);
        RegisteredUser contentManager = new RegisteredUser();
        testObjectsFactory.setFields(contentManager);

        FilmMisprintSignal filmMisprintSignal = testObjectsFactory.createFilmMisPrintSignalChecked(contentManager, film, user);

        filmMisprintSignalService.deleteFilmMisprintSignal(filmMisprintSignal.getId());
        Assert.assertFalse(errorSignalRepository.existsById(filmMisprintSignal.getId()));
    }

    @Test
    public void updateFilmMisprintSignal() throws Exception {
        Film film = new Film();
        testObjectsFactory.setFields(film);
        RegisteredUser user = new RegisteredUser();
        testObjectsFactory.setFields(user);
        RegisteredUser contentManager = new RegisteredUser();
        testObjectsFactory.setFields(contentManager);

        FilmMisprintSignal filmMisprintSignal = testObjectsFactory.createFilmMisPrintSignalChecked(contentManager, film, user);

        FilmMisprintSignalPutDTO put = new FilmMisprintSignalPutDTO();
        put.setFilmId(film.getId());

        filmMisprintSignalService.updateFilmMisprintSignal(filmMisprintSignal.getId(), put);

        FilmMisprintSignalReadDTO read = filmMisprintSignalService.updateFilmMisprintSignal(filmMisprintSignal.getId(), put);

        Assertions.assertThat(put)
                .isEqualToIgnoringGivenFields(read);

        filmMisprintSignal = (FilmMisprintSignal) errorSignalRepository.findById(filmMisprintSignal.getId()).get();

        Assertions.assertThat(filmMisprintSignal)
                .isEqualToIgnoringGivenFields(read, "film", "user", "contentManager");

        Assertions.assertThat(filmMisprintSignal.getFilm()).isEqualToIgnoringGivenFields(film);
        Assertions.assertThat(filmMisprintSignal.getContentManager())
                .isEqualToIgnoringGivenFields(contentManager, "likes", "reviews", "checks");
        Assertions.assertThat(filmMisprintSignal.getUser())
                .isEqualToIgnoringGivenFields(user, "likes", "reviews", "checks");
    }

    @Test
    public void testApproveFilmsMisPrintSignal() throws Exception {
        Film film = new Film();
        testObjectsFactory.setFields(film);
        RegisteredUser user = new RegisteredUser();
        testObjectsFactory.setFields(user);
        RegisteredUser contentManager = new RegisteredUser();
        testObjectsFactory.setFields(contentManager);

        FilmMisprintSignal filmMisprintSignal = testObjectsFactory.createFilmMisPrintSignalNonChecked(film, user);

        FilmMisprintSignalReadDTO read = testObjectsFactory.toRead(filmMisprintSignal, contentManager);

        FilmMisprintSignalReadDTO readAfterApprove = filmMisprintSignalService.approveFilmsMisPrintSignal(read);

        Assert.assertNull(readAfterApprove.getFixDate());
        Assert.assertEquals(readAfterApprove.getVerification(), VerificationStatusType.CHECKED);

        filmMisprintSignal = (FilmMisprintSignal) errorSignalRepository.findById(filmMisprintSignal.getId()).get();

        Assertions.assertThat(filmMisprintSignal)
                .isEqualToIgnoringGivenFields(readAfterApprove, "film", "user", "contentManager");

        Assertions.assertThat(filmMisprintSignal.getFilm()).isEqualToIgnoringGivenFields(film);
        Assertions.assertThat(filmMisprintSignal.getContentManager())
                .isEqualToIgnoringGivenFields(contentManager, "likes", "reviews", "checks");
        Assertions.assertThat(filmMisprintSignal.getUser())
                .isEqualToIgnoringGivenFields(user, "likes", "reviews", "checks");
    }

    @Test
    public void testApproveFilmsMisPrintSignalWithWrongStatusAndExceptException() throws Exception {
        Film film = new Film();
        testObjectsFactory.setFields(film);
        RegisteredUser user = new RegisteredUser();
        testObjectsFactory.setFields(user);
        RegisteredUser contentManager = new RegisteredUser();
        testObjectsFactory.setFields(contentManager);

        FilmMisprintSignal filmMisprintSignal = testObjectsFactory
                .createFilmMisPrintSignalChecked(contentManager, film, user);

        FilmMisprintSignalReadDTO read = testObjectsFactory.toRead(filmMisprintSignal, contentManager);

        assertThrows(EntityStatusException.class, () -> {
            filmMisprintSignalService.approveFilmsMisPrintSignal(read);
        });
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetFilmsMisPrintSignalByWrongId() {
        filmMisprintSignalService.getFilmMisprintSignal(UUID.randomUUID());
    }
}
