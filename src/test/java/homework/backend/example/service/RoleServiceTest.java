package homework.backend.example.service;

import homework.backend.example.domain.content.Film;
import homework.backend.example.domain.content.Role;
import homework.backend.example.domain.persons.Actor;
import homework.backend.example.domain.persons.Person;
import homework.backend.example.domain.users.RegisteredUser;
import homework.backend.example.dto.role.*;
import homework.backend.example.exception.EntityNotFoundException;
import homework.backend.example.repository.actor.ActorRepository;
import homework.backend.example.repository.role.RoleRepository;
import homework.backend.example.service.RoleService;
import homework.backend.example.testfactory.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Sql(statements = { "delete from rating",
                    "delete from role",
                    "delete from person",
                    "delete from film"},
                    executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class RoleServiceTest {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RoleService roleService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private ActorRepository actorRepository;

    @Test
    public void testGetRoleExt() throws Exception {
        Person person = new Person();
        testObjectsFactory.setFields(person);
        Film film = new Film();
        testObjectsFactory.setFields(film);
        Actor actor = testObjectsFactory.getActor(person);

        Role role = testObjectsFactory.getRole(actor, film);

        RoleReadExtendedDTO readExtendDTO = roleService.getRoleExt(role.getId());
        Assertions.assertThat(role).isEqualToIgnoringGivenFields(readExtendDTO, "actor", "film");
        Assertions.assertThat(role.getActor()).isEqualToIgnoringGivenFields(actor, "person");
        Assertions.assertThat(actor.getPerson()).isEqualToIgnoringGivenFields(person);
        Assertions.assertThat(role.getFilm()).isEqualToIgnoringGivenFields(film);
    }

    @Test
    public void testGetRole() throws Exception {
        Person person = new Person();
        testObjectsFactory.setFields(person);
        Film film = new Film();
        testObjectsFactory.setFields(film);
        Actor actor = testObjectsFactory.getActor(person);

        Role role = testObjectsFactory.getRole(actor, film);

        RoleReadDTO read = roleService.getRole(role.getId());
        Assertions.assertThat(role).isEqualToIgnoringGivenFields(read, "actor", "film");
        Assertions.assertThat(role.getActor()).isEqualToIgnoringGivenFields(actor);
        Assertions.assertThat(role.getFilm()).isEqualToIgnoringGivenFields(film);
    }

    @Test
    public void testCreateRole() throws Exception {
        Person person = new Person();
        testObjectsFactory.setFields(person);
        Film film = new Film();
        testObjectsFactory.setFields(film);
        Actor actor = testObjectsFactory.getActor(person);

        RoleCreateDTO create = testObjectsFactory.getRoleCreateDTO(actor.getId(), film.getId());

        RoleReadDTO read = roleService.createRole(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Role role = roleRepository.findById(read.getId()).get();
        Assertions.assertThat(role).isEqualToIgnoringGivenFields(read, "actor", "film");
        Assertions.assertThat(role.getActor()).isEqualToIgnoringGivenFields(actor);
        Assertions.assertThat(role.getFilm()).isEqualToIgnoringGivenFields(film);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateRoleWithWrongFilm() throws Exception {
        Film film = new Film();
        testObjectsFactory.setFields(film);
        RoleCreateDTO create = testObjectsFactory.getRoleCreateDTO(UUID.randomUUID(), film.getId());

        roleService.createRole(create);
    }

    @Test
    public void testPatchRole() throws Exception {
        Person person = new Person();
        testObjectsFactory.setFields(person);
        Film film = new Film();
        testObjectsFactory.setFields(film);
        Actor actor = testObjectsFactory.getActor(person);

        Role role = testObjectsFactory.getRole(actor, film);

        RolePatchDTO patch = testObjectsFactory.getRolePatch(actor.getId(), film.getId());

        RoleReadDTO read = roleService.patchRole(role.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        role = roleRepository.findById(read.getId()).get();
        Assertions.assertThat(role).isEqualToIgnoringGivenFields(read, "actor", "film");
        Assertions.assertThat(role.getActor()).isEqualToIgnoringGivenFields(actor);
        Assertions.assertThat(role.getFilm()).isEqualToIgnoringGivenFields(film);
    }

    @Test
    public void testPatchRoleEmptyPatch() throws Exception {
        Person person = new Person();
        testObjectsFactory.setFields(person);
        Film film = new Film();
        testObjectsFactory.setFields(film);
        Actor actor = testObjectsFactory.getActor(person);
        Role role = testObjectsFactory.getRole(actor, film);

        RolePatchDTO patch = new RolePatchDTO();

        RoleReadDTO read = roleService.patchRole(role.getId(), patch);

        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        Role roleAfterUpdate = roleRepository.findById(read.getId()).get();

        Assertions.assertThat(roleAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(role).isEqualToComparingFieldByField(roleAfterUpdate);
    }

    @Test
    public void testUpdateRole() throws Exception {
        Person person = new Person();
        testObjectsFactory.setFields(person);
        Film film = new Film();
        testObjectsFactory.setFields(film);
        Actor actor = testObjectsFactory.getActor(person);
        Role role = testObjectsFactory.getRole(actor, film);

        RolePutDTO put = testObjectsFactory.getRolePut(actor.getId(), film.getId());

        RoleReadDTO read = roleService.upDateRole(role.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        role = roleRepository.findById(read.getId()).get();
        Assertions.assertThat(role).isEqualToIgnoringGivenFields(read, "actor", "film");
        Assertions.assertThat(role.getActor()).isEqualToIgnoringGivenFields(actor);
        Assertions.assertThat(role.getFilm()).isEqualToIgnoringGivenFields(film);
    }

    @Test
    public void testDeleteFilm() throws Exception {
        Person person = new Person();
        testObjectsFactory.setFields(person);
        Film film = new Film();
        testObjectsFactory.setFields(film);
        Actor actor = testObjectsFactory.getActor(person);

        Role role = testObjectsFactory.getRole(actor, film);

        roleService.deleteRole(role.getId());
        Assert.assertFalse(roleRepository.existsById(role.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteFilmNotFound() {
        roleService.deleteRole(UUID.randomUUID());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetRoleByWrongId() {
        UUID wrongId = UUID.randomUUID();
        roleService.getRole(wrongId);
    }

    @Test
    public void testGetRolesWithEmptyFilter() throws Exception {
        Person person1 = new Person();
        testObjectsFactory.setFields(person1);
        Actor actor1 = testObjectsFactory.getActor(person1);
        Film film1 = new Film();
        testObjectsFactory.setFields(film1);
        Person person2 = new Person();
        testObjectsFactory.setFields(person2);
        Actor actor2 = testObjectsFactory.getActor(person2);
        Film film2 = new Film();
        testObjectsFactory.setFields(film2);

        Role role1 = testObjectsFactory.getRole(actor1, film1);
        Role role2 = testObjectsFactory.getRole(actor1, film2);
        Role role3 = testObjectsFactory.getRole(actor2, film2);

        RoleFilter filter = new RoleFilter();
        Assertions.assertThat(roleService.getRoles(filter)).extracting("id")
                .containsExactlyInAnyOrder(role1.getId(), role2.getId(), role3.getId());
    }

    @Test
    public void testGetRoleByActor() throws Exception {
        Person person1 = new Person();
        testObjectsFactory.setFields(person1);
        Actor actor1 = testObjectsFactory.getActor(person1);
        Film film1 = new Film();
        testObjectsFactory.setFields(film1);
        Person person2 = new Person();
        testObjectsFactory.setFields(person2);
        Actor actor2 = testObjectsFactory.getActor(person2);
        Film film2 = new Film();
        testObjectsFactory.setFields(film2);

        Role role1 = testObjectsFactory.getRole(actor1, film1);
        Role role2 = testObjectsFactory.getRole(actor1, film2);
        testObjectsFactory.getRole(actor2, film2);

        RoleFilter filter = new RoleFilter();
        filter.setActorId(actor1.getId());
        Assertions.assertThat(roleService.getRoles(filter)).extracting("id")
                .containsExactlyInAnyOrder(role1.getId(), role2.getId());
    }

    @Test
    public void testGetRoleByFilm() throws Exception {
        Person person1 = new Person();
        testObjectsFactory.setFields(person1);
        Actor actor1 = testObjectsFactory.getActor(person1);
        Film film1 = new Film();
        testObjectsFactory.setFields(film1);
        Person person2 = new Person();
        testObjectsFactory.setFields(person2);
        Actor actor2 = testObjectsFactory.getActor(person2);
        Film film2 = new Film();
        testObjectsFactory.setFields(film2);

        testObjectsFactory.getRole(actor1, film1);
        Role role2 = testObjectsFactory.getRole(actor1, film2);
        Role role3 = testObjectsFactory.getRole(actor2, film2);

        RoleFilter filter = new RoleFilter();
        filter.setFilmId(film2.getId());
        Assertions.assertThat(roleService.getRoles(filter)).extracting("id")
                .containsExactlyInAnyOrder(role2.getId(), role3.getId());
    }

    @Test
    public void testGetRoleByPay() throws Exception {
        Person person1 = new Person();
        testObjectsFactory.setFields(person1);
        Actor actor1 = testObjectsFactory.getActor(person1);
        Film film1 = new Film();
        testObjectsFactory.setFields(film1);
        Person person2 = new Person();
        testObjectsFactory.setFields(person2);
        Actor actor2 = testObjectsFactory.getActor(person2);
        Film film2 = new Film();
        testObjectsFactory.setFields(film2);

        testObjectsFactory.getRole(actor1, film1, 600L);
        Role role2 = testObjectsFactory.getRole(actor1, film2, 2000L);
        Role role3 = testObjectsFactory.getRole(actor2, film2, 1400L);

        RoleFilter filter = new RoleFilter();
        filter.setPay(900L);
        Assertions.assertThat(roleService.getRoles(filter)).extracting("id")
                .containsExactlyInAnyOrder(role2.getId(), role3.getId());
    }

    @Test
    public void testGetRoleByRating() throws Exception {
        Person person1 = new Person();
        testObjectsFactory.setFields(person1);
        Actor actor1 = testObjectsFactory.getActor(person1);
        Film film1 = new Film();
        testObjectsFactory.setFields(film1);
        Person person2 = new Person();
        testObjectsFactory.setFields(person2);
        Actor actor2 = testObjectsFactory.getActor(person2);
        Film film2 = new Film();
        testObjectsFactory.setFields(film2);

        testObjectsFactory.getRole(actor1, film1, 0.2);
        Role role2 = testObjectsFactory.getRole(actor1, film2, 6.0);
        Role role3 = testObjectsFactory.getRole(actor2, film2, 8.0);

        RoleFilter filter = new RoleFilter();
        filter.setAverageMark(5.0);
        Assertions.assertThat(roleService.getRoles(filter)).extracting("id")
                .containsExactlyInAnyOrder(role2.getId(), role3.getId());
    }

    @Test
    public void TestGetRoleByAllFilters() throws Exception {
        Person person1 = new Person();
        testObjectsFactory.setFields(person1);
        Actor actor1 = testObjectsFactory.getActor(person1);
        Film film1 = new Film();
        testObjectsFactory.setFields(film1);
        Person person2 = new Person();
        testObjectsFactory.setFields(person2);
        Actor actor2 = testObjectsFactory.getActor(person2);
        Film film2 = new Film();
        testObjectsFactory.setFields(film2);

        testObjectsFactory.getRole(actor1, film1, 4.9);
        testObjectsFactory.getRole(actor1, film2, 2000L, 6.0);
        Role role3 = testObjectsFactory.getRole(actor2, film2, 1600L, 9.9);

        RoleFilter filter = new RoleFilter();
        filter.setActorId(actor2.getId());
        filter.setFilmId(film2.getId());
        filter.setPay(1000L);
        filter.setAverageMark(5.0);
        Assertions.assertThat(roleService.getRoles(filter)).extracting("id")
                .containsExactlyInAnyOrder(role3.getId());
    }

    @Test
    public void testUpdateAverageMarkOfRole() throws Exception {
        Person person = new Person();
        testObjectsFactory.setFields(person);
        Actor actor = testObjectsFactory.getActor(person);
        Film film = new Film();
        testObjectsFactory.setFields(film);
        RegisteredUser user = new RegisteredUser();
        testObjectsFactory.setFields(user);
        Role role =  testObjectsFactory.getRoleWithoutRating(actor, film);

        testObjectsFactory.createRoleRating(user, role, 6.0);
        testObjectsFactory.createRoleRating(user, role, 4.0);

        roleService.updateAverageMarkOfRole(role.getId());
        role = roleRepository.findById(role.getId()).get();
        Assert.assertEquals(5.0, role.getAverageMark(), Double.MIN_NORMAL);
    }
}
