package homework.backend.example.service;

import homework.backend.example.domain.content.Film;
import homework.backend.example.domain.enums.VerificationStatusType;
import homework.backend.example.domain.massages.errormassages.misprintsignal.FilmMisprintSignal;
import homework.backend.example.domain.users.RegisteredUser;
import homework.backend.example.dto.film.FilmCreateDTO;
import homework.backend.example.dto.film.FilmPatchDTO;
import homework.backend.example.dto.film.FilmPutDTO;
import homework.backend.example.dto.film.FilmReadDTO;
import homework.backend.example.dto.misprintsignal.FilmMisprintSignalReadDTO;
import homework.backend.example.exception.EntityNotFoundException;
import homework.backend.example.exception.EntityStatusException;
import homework.backend.example.repository.film.FilmRepository;
import homework.backend.example.testfactory.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Sql(statements = { "delete from rating",
                    "delete from registered_user",
                    "delete from film"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class FilmServiceTest {

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private FilmService filmService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetFilm() throws Exception {
        Film film = new Film();
        testObjectsFactory.setFields(film);

        FilmReadDTO readDTO = filmService.getFilm(film.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(film);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetFilmByWrongId() {
        filmService.getFilm(UUID.randomUUID());
    }

    @Test
    public void testCreateFilm() throws Exception {
        FilmCreateDTO create = new FilmCreateDTO();
        testObjectsFactory.setFields(create);

        FilmReadDTO read = filmService.createFilm(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Film film = filmRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(film);
    }

    @Test
    public void testPatchFilm() throws Exception {
        Film film = new Film();
        testObjectsFactory.setFields(film);

        FilmPatchDTO patch = new FilmPatchDTO();
        testObjectsFactory.setFields(patch);

        FilmReadDTO read = filmService.patchFilm(film.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        film = filmRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(film);
    }

    @Test
    public void testPatchFilmEmptyPatch() throws Exception {
        Film film = new Film();
        testObjectsFactory.setFields(film);

        FilmPatchDTO patch = new FilmPatchDTO();
        FilmReadDTO read = filmService.patchFilm(film.getId(), patch);

        Assertions.assertThat(film).hasNoNullFieldsOrProperties();
        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        Film filmAfterUpdate = filmRepository.findById(read.getId()).get();

        Assertions.assertThat(filmAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(film).isEqualToComparingFieldByField(filmAfterUpdate);
    }

    @Test
    public void testPutFilm() throws Exception {
        Film film = new Film();
        testObjectsFactory.setFields(film);

        FilmPutDTO putDTO = new FilmPutDTO();

        FilmReadDTO read = filmService.upDateFilm(film.getId(), putDTO);

        Assertions.assertThat(putDTO).isEqualToComparingFieldByField(read);

        film = filmRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(film);
    }

    @Test
    public void testDeleteFilm() throws Exception {
        Film film = new Film();
        testObjectsFactory.setFields(film);

        filmService.deleteFilm(film.getId());
        Assert.assertFalse(filmRepository.existsById(film.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteFilmNotFound() {
        filmService.deleteFilm(UUID.randomUUID());
    }

    @Test
    public void testUpdateAverageMarkOfFilm() throws Exception {
        RegisteredUser user = new RegisteredUser();
        testObjectsFactory.setFields(user);
        Film film = new Film();
        testObjectsFactory.setFields(film);

        testObjectsFactory.createFilmRating(user, film, 4.0);
        testObjectsFactory.createFilmRating(user, film, 10.0);

        filmService.updateAverageMarkOfFilm(film.getId());
        film = filmRepository.findById(film.getId()).get();
        Assert.assertEquals(7.0, film.getAverageMark(), Double.MIN_NORMAL);
    }

    @Test
    public void fixFilmTest() throws Exception {
        Film film = new Film();
        testObjectsFactory.setFields(film);
        FilmMisprintSignalReadDTO read = new FilmMisprintSignalReadDTO();
        testObjectsFactory.setFields(read);
        read.setFilmId(film.getId());

        String oldDescription = film.getDescription();
        System.out.println(oldDescription);
        filmService.fixFilm(read);

        Film filmAfterReload = filmRepository.findById(film.getId()).get();
        String newDescription = filmAfterReload.getDescription();
        System.out.println(newDescription);
        Assert.assertTrue(filmAfterReload.getDescription().contains(read.getCorrectString()));
        Assert.assertNotEquals(newDescription, oldDescription);
    }
}
