package homework.backend.example.service;

import homework.backend.example.domain.persons.Actor;
import homework.backend.example.domain.persons.Person;
import homework.backend.example.dto.actor.ActorCreateDTO;
import homework.backend.example.dto.actor.ActorPatchDTO;
import homework.backend.example.dto.actor.ActorPutDTO;
import homework.backend.example.dto.actor.ActorReadDTO;
import homework.backend.example.exception.EntityNotFoundException;
import homework.backend.example.repository.actor.ActorRepository;
import homework.backend.example.testfactory.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Sql(statements = "delete from actor", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class ActorServiceTest {
    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private ActorService actorService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetActor() throws Exception {
        Person person = new Person();
        testObjectsFactory.setFields(person);
        Actor actor = testObjectsFactory.getActor(person);

        ActorReadDTO read = actorService.getActor(actor.getId());
        Assertions.assertThat(actor).isEqualToIgnoringGivenFields(read, "person");
        Assertions.assertThat(actor.getPerson()).isEqualToComparingOnlyGivenFields(person);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetActorByWrongId() {
        actorService.getActor(UUID.randomUUID());
    }

    @Test
    public void testCreateActor() throws Exception {
        Person person = new Person();
        testObjectsFactory.setFields(person);
        ActorCreateDTO create = testObjectsFactory.getActorCreateDTO(person.getId());

        ActorReadDTO read = actorService.createActor(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Actor actor = actorRepository.findById(read.getId()).get();
        Assertions.assertThat(actor).isEqualToIgnoringGivenFields(read, "person");
        Assertions.assertThat(actor.getPerson()).isEqualToComparingOnlyGivenFields(person);
    }

    @Test
    public void testPatchActor() throws Exception {
        Person person = new Person();
        testObjectsFactory.setFields(person);
        Actor actor = testObjectsFactory.getActor(person);

        ActorPatchDTO patch = testObjectsFactory.getActorPatchDTO(person.getId());

        ActorReadDTO read = actorService.patchActor(actor.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        actor = actorRepository.findById(read.getId()).get();
        Assertions.assertThat(actor).isEqualToIgnoringGivenFields(read, "person");
        Assertions.assertThat(actor.getPerson()).isEqualToComparingOnlyGivenFields(person);
    }

    @Test
    public void testPatchActorEmptyPatch() throws Exception {
        Person person = new Person();
        testObjectsFactory.setFields(person);
        Actor actor = testObjectsFactory.getActor(person);

        ActorPatchDTO patch = new ActorPatchDTO();
        ActorReadDTO read = actorService.patchActor(actor.getId(), patch);

        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        Actor actorAfterUpdate = actorRepository.findById(read.getId()).get();

        Assertions.assertThat(actorAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(actor).isEqualToComparingFieldByField(actorAfterUpdate);
    }

    @Test
    public void testPutActor() throws Exception {
        Person person = new Person();
        testObjectsFactory.setFields(person);
        Actor actor = testObjectsFactory.getActor(person);
        ActorPutDTO put = new ActorPutDTO();
        put.setPersonId(person.getId());

        ActorReadDTO read = actorService.upDateActor(actor.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        actor = actorRepository.findById(read.getId()).get();
        Assertions.assertThat(actor).isEqualToIgnoringGivenFields(read, "person");
        Assertions.assertThat(actor.getPerson()).isEqualToComparingOnlyGivenFields(person);
    }

    @Test
    public void testDeleteNews() throws Exception {
        Person person = new Person();
        testObjectsFactory.setFields(person);
        Actor actor = testObjectsFactory.getActor(person);

        actorService.deleteActor(actor.getId());
        Assert.assertFalse(actorRepository.existsById(actor.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteNewsNotFound() {
        actorService.deleteActor(UUID.randomUUID());
    }
}
