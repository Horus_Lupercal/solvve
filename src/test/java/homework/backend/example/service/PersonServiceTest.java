package homework.backend.example.service;

import homework.backend.example.domain.persons.Person;
import homework.backend.example.dto.person.PersonCreateDTO;
import homework.backend.example.dto.person.PersonPatchDTO;
import homework.backend.example.dto.person.PersonPutDTO;
import homework.backend.example.dto.person.PersonReadDTO;
import homework.backend.example.exception.EntityNotFoundException;
import homework.backend.example.repository.person.PersonRepository;
import homework.backend.example.testfactory.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Sql(statements = "delete from person", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class PersonServiceTest {

    @Autowired
    private PersonService personService;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetPerson() throws Exception {
        Person person = new Person();
        testObjectsFactory.setFields(person);

        PersonReadDTO readDTO = personService.getPerson(person.getId());

        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(person);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetPersonByWrongId() {
        personService.getPerson(UUID.randomUUID());
    }

    @Test
    public void testCreatePerson() throws Exception {
        PersonCreateDTO createDTO = new PersonCreateDTO();
        testObjectsFactory.setFields(createDTO);

        PersonReadDTO readDTO = personService.createPerson(createDTO);

        Assertions.assertThat(createDTO).isEqualToComparingFieldByField(readDTO);
        Assert.assertNotNull(readDTO.getId());

        Person person = personRepository.findById(readDTO.getId()).get();
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(person);
    }

    @Test
    public void testPatchPerson() throws Exception {
        Person person = new Person();
        testObjectsFactory.setFields(person);

        PersonPatchDTO patch = new PersonPatchDTO();
        testObjectsFactory.setFields(patch);

        PersonReadDTO read = personService.patchPerson(person.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        person = personRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(person);
    }

    @Test
    public void testPatchPersonEmptyPatch() throws Exception {
        Person person = new Person();
        testObjectsFactory.setFields(person);

        PersonPatchDTO patch = new PersonPatchDTO();

        PersonReadDTO read = personService.patchPerson(person.getId(), patch);

        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        Person personAfterUpdate = personRepository.findById(read.getId()).get();

        Assertions.assertThat(personAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(person).isEqualToComparingFieldByField(personAfterUpdate);
    }

    @Test
    public void testPutPerson() throws Exception {
        Person person = new Person();
        testObjectsFactory.setFields(person);

        PersonPutDTO put = new PersonPutDTO();

        PersonReadDTO read = personService.upDatePerson(person.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        person = personRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(person);
    }

    @Test
    public void testDeletePerson() {
        Person person = testObjectsFactory.getPerson();

        personService.deletePerson(person.getId());
        Assert.assertFalse(personRepository.existsById(person.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeletePersonNotFound() {
        personService.deletePerson(UUID.randomUUID());
    }
}
