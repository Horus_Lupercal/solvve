package homework.backend.example.service;

import homework.backend.example.domain.content.News;
import homework.backend.example.dto.news.NewsCreateDTO;
import homework.backend.example.dto.news.NewsPatchDTO;
import homework.backend.example.dto.news.NewsPutDTO;
import homework.backend.example.dto.news.NewsReadDTO;
import homework.backend.example.exception.EntityNotFoundException;
import homework.backend.example.repository.news.NewsRepository;
import homework.backend.example.testfactory.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Sql(statements = "delete from news", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class NewsServiceTest {

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private NewsService newsService;

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Test
    public void testGetNews() throws Exception {
        News news = new News();
        testObjectsFactory.setFields(news);

        NewsReadDTO read = newsService.getNews(news.getId());
        Assertions.assertThat(news).isEqualToIgnoringGivenFields(read, "registeredUser");
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetNewsByWrongId() {
        newsService.getNews(UUID.randomUUID());
    }

    @Test
    public void testCreateNews() throws Exception {
        NewsCreateDTO create = new NewsCreateDTO();
        testObjectsFactory.setFields(create);

        NewsReadDTO read = newsService.createNews(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        News news = newsRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(news);
    }

    @Test
    public void testPatchNews() throws Exception {
        News news = new News();
        testObjectsFactory.setFields(news);

        NewsPatchDTO patch = new NewsPatchDTO();
        testObjectsFactory.setFields(patch);

        NewsReadDTO read = newsService.patchNews(news.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        news = newsRepository.findById(read.getId()).get();
        Assertions.assertThat(news).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchNewsEmptyPatch() throws Exception {
        News news = new News();
        testObjectsFactory.setFields(news);

        NewsPatchDTO patch = new NewsPatchDTO();
        NewsReadDTO read = newsService.patchNews(news.getId(), patch);

        Assertions.assertThat(read).hasNoNullFieldsOrProperties();

        News filmAfterUpdate = newsRepository.findById(read.getId()).get();

        Assertions.assertThat(filmAfterUpdate).hasNoNullFieldsOrProperties();

        Assertions.assertThat(news).isEqualToComparingFieldByField(filmAfterUpdate);
    }

    @Test
    public void testPutNews() throws Exception {
        News news = new News();
        testObjectsFactory.setFields(news);

        NewsPutDTO putDTO = new NewsPutDTO();

        NewsReadDTO read = newsService.upDateNews(news.getId(), putDTO);

        Assertions.assertThat(putDTO).isEqualToComparingFieldByField(read);

        news = newsRepository.findById(read.getId()).get();
        Assertions.assertThat(news).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testDeleteNews() throws Exception {
        News news = new News();
        testObjectsFactory.setFields(news);

        newsService.deleteNews(news.getId());
        Assert.assertFalse(newsRepository.existsById(news.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteNewsNotFound() {
        newsService.deleteNews(UUID.randomUUID());
    }
}
