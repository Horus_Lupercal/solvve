package homework.backend.example.job;

import homework.backend.example.domain.content.Film;
import homework.backend.example.domain.content.Role;
import homework.backend.example.domain.persons.Actor;
import homework.backend.example.domain.persons.Person;
import homework.backend.example.domain.users.RegisteredUser;
import homework.backend.example.repository.film.FilmRepository;
import homework.backend.example.repository.role.RoleRepository;
import homework.backend.example.service.FilmService;
import homework.backend.example.service.RoleService;
import homework.backend.example.testfactory.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Sql(statements = { "delete from rating",
                    "delete from registered_user",
                    "delete from film"},
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class UpdateAverageMarkOfFilmsJodTest {

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private FilmRepository filmRepository;

    @SpyBean
    private FilmService filmService;

    @Autowired
    private UpdateAverageMarkOfFilmsJob updateAverageMarkOfFilmsJob;

    @Test
    public void testUpdateAverageMarkOfFilms() throws Exception {
        Film film = new Film();
        film = filmRepository.save(film);
        RegisteredUser user = new RegisteredUser();
        testObjectsFactory.setFields(user);

        testObjectsFactory.createFilmRating(user, film, 2.0);
        testObjectsFactory.createFilmRating(user, film, 10.0);
        updateAverageMarkOfFilmsJob.updateAverageMarkOfFilms();

        film = filmRepository.findById(film.getId()).get();
        Assert.assertEquals(6.0, film.getAverageMark(), Double.MIN_NORMAL);
    }

    @Test
    public void testFilmsUpdatedIndependently() throws Exception {
        Film film = new Film();
        film = filmRepository.save(film);
        RegisteredUser user = new RegisteredUser();
        testObjectsFactory.setFields(user);

        testObjectsFactory.createFilmRating(user, film, 6.0);
        testObjectsFactory.createFilmRating(user, film, 4.0);

        UUID[] failedId = new UUID[1];
        Mockito.doAnswer(invocationOnMock -> {
            if (failedId[0] == null) {
                failedId[0] = invocationOnMock.getArgument(0);
                throw new RuntimeException();
            }
            return invocationOnMock.callRealMethod();
        }).when(filmService).updateAverageMarkOfFilm(Mockito.any());

        updateAverageMarkOfFilmsJob.updateAverageMarkOfFilms();

        for (Film f : filmRepository.findAll()) {
            if (f.getId().equals(failedId[0])) {
                Assert.assertNull(f.getAverageMark());
            } else {
                Assert.assertNotNull(f.getAverageMark());
            }
        }
    }
}
