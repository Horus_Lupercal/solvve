package homework.backend.example.job;

import homework.backend.example.domain.content.Film;
import homework.backend.example.domain.content.Role;
import homework.backend.example.domain.persons.Actor;
import homework.backend.example.domain.persons.Person;
import homework.backend.example.domain.users.RegisteredUser;
import homework.backend.example.repository.role.RoleRepository;
import homework.backend.example.service.RoleService;
import homework.backend.example.testfactory.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Sql(statements = { "delete from rating",
                    "delete from role",
                    "delete from registered_user",
                    "delete from film"},
                    executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class UpdateAverageMarkOfRolesJobTest {

    @Autowired
    private TestObjectsFactory testObjectsFactory;

    @Autowired
    private RoleRepository roleRepository;

    @SpyBean
    private RoleService roleService;

    @Autowired
    private UpdateAverageMarkOfRolesJob updateAverageMarkOfRolesJob;

    @Test
    public void testUpdateAverageMarkOfFilms() throws Exception {
        RegisteredUser user = new RegisteredUser();
        testObjectsFactory.setFields(user);
        Role role = new Role();
        role = roleRepository.save(role);

        testObjectsFactory.createRoleRating(user, role, 2.0);
        testObjectsFactory.createRoleRating(user, role, 10.0);
        updateAverageMarkOfRolesJob.updateAverageMarkOfRoles();

        role = roleRepository.findById(role.getId()).get();
        Assert.assertEquals(6.0, role.getAverageMark(), Double.MIN_NORMAL);

    }

    @Test
    public void testRolesUpdatedIndependently() throws Exception {
        Person person = new Person();
        testObjectsFactory.setFields(person);
        Actor actor = testObjectsFactory.getActor(person);
        Film film = new Film();
        testObjectsFactory.setFields(film);
        RegisteredUser user = new RegisteredUser();
        testObjectsFactory.setFields(user);
        Role role = testObjectsFactory.getRoleWithoutRating(actor, film);

        testObjectsFactory.createRoleRating(user, role, 6.0);
        testObjectsFactory.createRoleRating(user, role, 4.0);

        UUID[] failedId = new UUID[1];
        Mockito.doAnswer(invocationOnMock -> {
            if (failedId[0] == null) {
                failedId[0] = invocationOnMock.getArgument(0);
                throw new RuntimeException();
            }
            return invocationOnMock.callRealMethod();
        }).when(roleService).updateAverageMarkOfRole(Mockito.any());

        updateAverageMarkOfRolesJob.updateAverageMarkOfRoles();

        for (Role r : roleRepository.findAll()) {
            if (r.getId().equals(failedId[0])) {
                Assert.assertNull(r.getAverageMark());
            } else {
                Assert.assertNotNull(r.getAverageMark());
            }
        }
    }
}
