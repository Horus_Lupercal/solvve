package homework.backend.example.liquibase;

import homework.backend.example.repository.actor.ActorRepository;
import homework.backend.example.repository.errorsignal.ErrorSignalRepository;
import homework.backend.example.repository.film.FilmRepository;
import homework.backend.example.repository.genre.GenreRepository;
import homework.backend.example.repository.like.LikeRepository;
import homework.backend.example.repository.news.NewsRepository;
import homework.backend.example.repository.person.PersonRepository;
import homework.backend.example.repository.rating.RatingRepository;
import homework.backend.example.repository.registereduser.RegisteredUserRepository;
import homework.backend.example.repository.review.ReviewRepository;
import homework.backend.example.repository.reviewcheck.ReviewCheckRepository;
import homework.backend.example.repository.role.RoleRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@TestPropertySource(properties = "spring.liquibase.change-log=classpath:db/changelog/db.changelog-master.xml")
@Sql(statements = {"delete from error_signal",
        "delete from review_check",
        "delete from review",
        "delete from rating",
        "delete from like",
        "delete from role",
        "delete from news",
        "delete from genre",
        "delete from registered_user",
        "delete from actor",
        "delete from person",
        "delete from film"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class LiquibaseLoadDataTest {
    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private RegisteredUserRepository registeredUserRepository;

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private NewsRepository newsRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private LikeRepository likeRepository;

    @Autowired
    private RatingRepository ratingRepository;

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private ReviewCheckRepository reviewCheckRepository;

    @Autowired
    private ErrorSignalRepository errorSignalRepository;

    @Test
    public void testDateLoaded() {
        Assert.assertTrue(personRepository.count() > 0);
        Assert.assertTrue(actorRepository.count() > 0);
        Assert.assertTrue(registeredUserRepository.count() > 0);
        Assert.assertTrue(genreRepository.count() > 0);
        Assert.assertTrue(newsRepository.count() > 0);
        Assert.assertTrue(roleRepository.count() > 0);
        Assert.assertTrue(likeRepository.count() > 0);
        Assert.assertTrue(ratingRepository.count() > 0);
        Assert.assertTrue(reviewRepository.count() > 0);
        Assert.assertTrue(errorSignalRepository.count() > 0);
        Assert.assertTrue(reviewCheckRepository.count() > 0);
        Assert.assertTrue(filmRepository.count() > 0);
    }
}
