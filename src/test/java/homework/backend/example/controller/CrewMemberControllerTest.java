package homework.backend.example.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import homework.backend.example.domain.persons.CrewMember;
import homework.backend.example.dto.crewmember.CrewMemberCreateDTO;
import homework.backend.example.dto.crewmember.CrewMemberPatchDTO;
import homework.backend.example.dto.crewmember.CrewMemberPutDTO;
import homework.backend.example.dto.crewmember.CrewMemberReadDTO;
import homework.backend.example.exception.EntityNotFoundException;
import homework.backend.example.service.CrewMemberService;
import homework.backend.example.testfactory.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CrewMemberController.class)
public class CrewMemberControllerTest {

    @MockBean
    private CrewMemberService crewMemberService;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    private TestObjectsFactory testObjectsFactory = new TestObjectsFactory();

    @Test
    public void testGetCrewMember() throws Exception {
        CrewMemberReadDTO read = new CrewMemberReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(crewMemberService.getCrewMember(read.getId())).thenReturn(read);

        String resultJson = mvc.perform(get ("/api/v1/crew/{id}", read.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CrewMemberReadDTO actualCrewMember = objectMapper.readValue(resultJson, CrewMemberReadDTO.class);
        Assertions.assertThat(actualCrewMember).isEqualToComparingFieldByField(read);
        Mockito.verify(crewMemberService).getCrewMember(read.getId());
    }

    @Test
    public void testGetCrewMemberByWrongID() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(CrewMember.class, wrongId);
        Mockito.when(crewMemberService.getCrewMember(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/crew/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateCrewMember() throws Exception {
        CrewMemberCreateDTO create = new CrewMemberCreateDTO();
        testObjectsFactory.setFields(create);

        CrewMemberReadDTO read = new CrewMemberReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(crewMemberService.createCrewMember(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/crew")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CrewMemberReadDTO actualCrewMember = objectMapper.readValue(resultJson, CrewMemberReadDTO.class);
        Assertions.assertThat(actualCrewMember).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchCrewMember() throws Exception {
        CrewMemberPatchDTO patch = new CrewMemberPatchDTO();
        testObjectsFactory.setFields(patch);

        CrewMemberReadDTO read = new CrewMemberReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(crewMemberService.patchCrewMember(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/crew/{id}", read.getId())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        CrewMemberReadDTO actualCrewMember = objectMapper.readValue(resultJson, CrewMemberReadDTO.class);
        Assertions.assertThat(actualCrewMember).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testDeleteCrew() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/crew/{id}", id)).andExpect(status().isOk());

        Mockito.verify(crewMemberService).deleteCrewMember(id);
    }

    @Test
    public void testPutCrew() throws Exception {
        CrewMemberPutDTO put = new CrewMemberPutDTO();
        testObjectsFactory.setFields(put);

        CrewMemberReadDTO read = new CrewMemberReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(crewMemberService.updateCrewMember(read.getId(), put)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/crew/{id}", read.getId())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        CrewMemberReadDTO actualCrewMember = objectMapper.readValue(resultJson, CrewMemberReadDTO.class);
        Assert.assertEquals(read, actualCrewMember);
    }
}
