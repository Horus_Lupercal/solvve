package homework.backend.example.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import homework.backend.example.dto.misprintsignal.FilmMisprintSignalReadDTO;
import homework.backend.example.service.FilmMisprintSignalService;
import homework.backend.example.testfactory.TestObjectsFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = FilmMisprintAnswerController.class)
public class FilmMisprintAnswerControllerTest {

    @MockBean
    private FilmMisprintSignalService filmMisprintSignalService;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    private TestObjectsFactory testObjectsFactory = new TestObjectsFactory();

    @Test
    public void testFixFilm() throws Exception {
        FilmMisprintSignalReadDTO read = new FilmMisprintSignalReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(filmMisprintSignalService.approveFilmsMisPrintSignal(read)).thenReturn(read);

        String resultJson = mvc.perform(patch("/v1/api/filmMisPrintAnswers")
                .content(objectMapper.writeValueAsString(read))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        FilmMisprintSignalReadDTO actualFilmMisprintSignalReadDTO = objectMapper
                .readValue(resultJson, FilmMisprintSignalReadDTO.class);
        Assert.assertEquals(read, actualFilmMisprintSignalReadDTO);
    }
}
