package homework.backend.example.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import homework.backend.example.domain.persons.Person;
import homework.backend.example.dto.person.PersonCreateDTO;
import homework.backend.example.dto.person.PersonPatchDTO;
import homework.backend.example.dto.person.PersonPutDTO;
import homework.backend.example.dto.person.PersonReadDTO;
import homework.backend.example.exception.EntityNotFoundException;
import homework.backend.example.service.PersonService;
import homework.backend.example.testfactory.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = PersonController.class)
public class PersonControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private PersonService personService;

    private TestObjectsFactory testObjectsFactory = new TestObjectsFactory();

    @Test
    public void testGetPerson() throws Exception {
        PersonReadDTO read = new PersonReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(personService.getPerson(read.getId())).thenReturn(read);

        String resultJson = mvc.perform(get("/api/v1/persons/{id}", read.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        PersonReadDTO actualPerson = objectMapper.readValue(resultJson, PersonReadDTO.class);
        Assertions.assertThat(actualPerson).isEqualToComparingFieldByField(read);

        Mockito.verify(personService).getPerson(read.getId());
    }

    @Test
    public void testCreatePerson() throws Exception {
        PersonCreateDTO create = new PersonCreateDTO();
        testObjectsFactory.setFields(create);

        PersonReadDTO read = new PersonReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(personService.createPerson(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/persons")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PersonReadDTO actualPerson = objectMapper.readValue(resultJson, PersonReadDTO.class);
        Assertions.assertThat(actualPerson).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchPerson() throws Exception {
        PersonPatchDTO patch = new PersonPatchDTO();
        testObjectsFactory.setFields(patch);

        PersonReadDTO read = new PersonReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(personService.patchPerson(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/persons/{id}", read.getId())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PersonReadDTO actualPerson = objectMapper.readValue(resultJson, PersonReadDTO.class);
        Assert.assertEquals(read, actualPerson);
    }

    @Test
    public void testPutPerson() throws Exception {
        PersonPutDTO put = new PersonPutDTO();
        testObjectsFactory.setFields(put);

        PersonReadDTO read = new PersonReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(personService.upDatePerson(read.getId(), put)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/persons/{id}", read.getId())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PersonReadDTO actualPerson = objectMapper.readValue(resultJson, PersonReadDTO.class);
        Assertions.assertThat(read).isEqualToComparingFieldByField(actualPerson);
    }

    @Test
    public void testDeletePerson() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/persons/{id}", id)).andExpect(status().isOk());

        Mockito.verify(personService).deletePerson(id);
    }

    @Test
    public void testGetPersonByWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Person.class, wrongId);

        Mockito.when(personService.getPerson(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/persons/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }
}
