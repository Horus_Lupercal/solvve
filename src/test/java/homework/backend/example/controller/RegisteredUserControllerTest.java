package homework.backend.example.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import homework.backend.example.domain.users.RegisteredUser;
import homework.backend.example.dto.registreduser.RegisteredUserCreateDTO;
import homework.backend.example.dto.registreduser.RegisteredUserPatchDTO;
import homework.backend.example.dto.registreduser.RegisteredUserPutDTO;
import homework.backend.example.dto.registreduser.RegisteredUserReadDTO;
import homework.backend.example.exception.EntityNotFoundException;
import homework.backend.example.service.RegisteredUserService;
import homework.backend.example.testfactory.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = RegisteredUserController.class)
public class RegisteredUserControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private RegisteredUserService userService;

    private TestObjectsFactory testObjectsFactory = new TestObjectsFactory();

    @Test
    public void testGetPerson() throws Exception {
        RegisteredUserReadDTO read = new RegisteredUserReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(userService.getUser(read.getId())).thenReturn(read);

        String resultJson = mvc.perform(get("/api/v1/users/{id}", read.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        RegisteredUserReadDTO actualUser = objectMapper.readValue(resultJson, RegisteredUserReadDTO.class);
        Assertions.assertThat(actualUser).isEqualToComparingFieldByField(read);

        Mockito.verify(userService).getUser(read.getId());
    }

    @Test
    public void testCreatePerson() throws Exception {
        RegisteredUserCreateDTO create = new RegisteredUserCreateDTO();
        testObjectsFactory.setFields(create);

        RegisteredUserReadDTO read = new RegisteredUserReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(userService.createUser(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/users")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegisteredUserReadDTO actualUser = objectMapper.readValue(resultJson, RegisteredUserReadDTO.class);
        Assertions.assertThat(actualUser).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchPerson() throws Exception {
        RegisteredUserPatchDTO patch = new RegisteredUserPatchDTO();
        testObjectsFactory.setFields(patch);

        RegisteredUserReadDTO read = new RegisteredUserReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(userService.patchUser(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/users/{id}", read.getId())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegisteredUserReadDTO actualUser = objectMapper.readValue(resultJson, RegisteredUserReadDTO.class);
        Assert.assertEquals(read, actualUser);
    }

    @Test
    public void testPutPerson() throws Exception {
        RegisteredUserPutDTO put = new RegisteredUserPutDTO();
        testObjectsFactory.setFields(put);

        RegisteredUserReadDTO read = new RegisteredUserReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(userService.upDateUser(read.getId(), put)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/users/{id}", read.getId())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RegisteredUserReadDTO actualUser = objectMapper.readValue(resultJson, RegisteredUserReadDTO.class);
        Assertions.assertThat(read).isEqualToComparingFieldByField(actualUser);
    }

    @Test
    public void testDeletePerson() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/users/{id}", id)).andExpect(status().isOk());

        Mockito.verify(userService).deleteUser(id);
    }

    @Test
    public void testGetPersonByWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(RegisteredUser.class, wrongId);

        Mockito.when(userService.getUser(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/users/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }
}
