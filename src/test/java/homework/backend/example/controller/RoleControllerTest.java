package homework.backend.example.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import homework.backend.example.domain.content.Role;
import homework.backend.example.dto.actor.ActorReadDTO;
import homework.backend.example.dto.film.FilmReadDTO;
import homework.backend.example.dto.role.*;
import homework.backend.example.exception.EntityNotFoundException;
import homework.backend.example.service.RoleService;
import homework.backend.example.testfactory.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = RoleController.class)
public class RoleControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private RoleService roleService;

    private TestObjectsFactory testObjectsFactory = new TestObjectsFactory();

    @Test
    public void testGetRoleExt() throws Exception {
        ActorReadDTO actor = new ActorReadDTO();
        testObjectsFactory.setFields(actor);
        FilmReadDTO film = new FilmReadDTO();
        testObjectsFactory.setFields(film);
        RoleReadExtendedDTO roleExtend = new RoleReadExtendedDTO();
        testObjectsFactory.setFields(roleExtend);
        roleExtend.setActor(actor);
        roleExtend.setFilm(film);

        Mockito.when(roleService.getRoleExt(roleExtend.getId())).thenReturn(roleExtend);

        String resultJson = mvc.perform(get("/api/v1/roles/extended/{id}", roleExtend.getId()))
                .andExpect(status().isOk()).andReturn()
                .getResponse().getContentAsString();

        RoleReadExtendedDTO actualRoleExtend = objectMapper.readValue(resultJson, RoleReadExtendedDTO.class);
        Assertions.assertThat(roleExtend).isEqualToComparingFieldByField(actualRoleExtend);
    }

    @Test
    public void testGetRoleByWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Role.class, wrongId);

        Mockito.when(roleService.getRole(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/roles/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetRole() throws Exception {
        RoleReadDTO role = new RoleReadDTO();
        testObjectsFactory.setFields(role);

        Mockito.when(roleService.getRole(role.getId())).thenReturn(role);

        String resultJson = mvc.perform(get("/api/v1/roles/{id}", role.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        RoleReadDTO actualRole = objectMapper.readValue(resultJson, RoleReadDTO.class);
        Assertions.assertThat(actualRole).isEqualToComparingFieldByField(role);

        Mockito.verify(roleService).getRole(role.getId());
    }

    @Test
    public void testCreateRole() throws Exception {
        RoleCreateDTO create = new RoleCreateDTO();
        testObjectsFactory.setFields(create);

        RoleReadDTO read = new RoleReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(roleService.createRole(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/roles")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReadDTO actualRole = objectMapper.readValue(resultJson, RoleReadDTO.class);
        Assertions.assertThat(actualRole).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchRole() throws Exception {
        RolePatchDTO patch = new RolePatchDTO();
        testObjectsFactory.setFields(patch);

        RoleReadDTO read = new RoleReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(roleService.patchRole(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/roles/{id}", read.getId())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReadDTO actualRole = objectMapper.readValue(resultJson, RoleReadDTO.class);
        Assert.assertEquals(read, actualRole);
    }

    @Test
    public void testPutRole() throws Exception {
        RolePutDTO put = new RolePutDTO();
        testObjectsFactory.setFields(put);

        RoleReadDTO read = new RoleReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(roleService.upDateRole(read.getId(), put)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/roles/{id}", read.getId())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReadDTO actualRole = objectMapper.readValue(resultJson, RoleReadDTO.class);
        Assert.assertEquals(read, actualRole);
    }

    @Test
    public void testDeleteRole() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/roles/{id}", id)).andExpect(status().isOk());

        Mockito.verify(roleService).deleteRole(id);
    }

    @Test
    public void testGetRoles() throws Exception {
        RoleFilter filter = new RoleFilter();
        filter.setActorId(UUID.randomUUID());
        filter.setFilmId(UUID.randomUUID());
        filter.setPay(2000L);
        filter.setAverageMark(9.9);

        RoleReadDTO read = new RoleReadDTO();
        read.setActorId(filter.getActorId());
        read.setFilmId(filter.getFilmId());
        read.setId(UUID.randomUUID());
        read.setPay(3000L);
        read.setAverageMark(10.0);

        List<RoleReadDTO> expectedResult = List.of(read);

        Mockito.when(roleService.getRoles(filter)).thenReturn(expectedResult);

        String resultJson = mvc.perform(get("/api/v1/roles")
                .param("actorId", filter.getActorId().toString())
                .param("filmId", filter.getFilmId().toString())
                .param("pay", filter.getPay().toString())
                .param("averageMark", filter.getAverageMark().toString()))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        List<RoleReadDTO> actualResult = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(expectedResult, actualResult);
    }
}
