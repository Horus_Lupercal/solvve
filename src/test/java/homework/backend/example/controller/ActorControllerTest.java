package homework.backend.example.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import homework.backend.example.domain.persons.Actor;
import homework.backend.example.dto.actor.ActorCreateDTO;
import homework.backend.example.dto.actor.ActorPatchDTO;
import homework.backend.example.dto.actor.ActorPutDTO;
import homework.backend.example.dto.actor.ActorReadDTO;
import homework.backend.example.exception.EntityNotFoundException;
import homework.backend.example.service.ActorService;
import homework.backend.example.testfactory.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ActorController.class)
public class ActorControllerTest {

    @MockBean
    private ActorService actorService;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    private TestObjectsFactory testObjectsFactory = new TestObjectsFactory();

    @Test
    public void testGetActor() throws Exception {
        ActorReadDTO read = new ActorReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(actorService.getActor(read.getId())).thenReturn(read);

        String resultJson = mvc.perform(get("/api/v1/actors/{id}", read.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        ActorReadDTO actualActor = objectMapper.readValue(resultJson, ActorReadDTO.class);
        Assertions.assertThat(actualActor).isEqualToComparingFieldByField(read);

        Mockito.verify(actorService).getActor(read.getId());
    }

    @Test
    public void testGetActorByWrongID() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Actor.class, wrongId);
        Mockito.when(actorService.getActor(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/actors/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateActor() throws Exception {
        ActorCreateDTO create = new ActorCreateDTO();
        testObjectsFactory.setFields(create);

        ActorReadDTO read = new ActorReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(actorService.createActor(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/actors")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ActorReadDTO actualActor = objectMapper.readValue(resultJson, ActorReadDTO.class);
        Assertions.assertThat(actualActor).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchActor() throws Exception {
        ActorPatchDTO patch = new ActorPatchDTO();
        testObjectsFactory.setFields(patch);

        ActorReadDTO read = new ActorReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(actorService.patchActor(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/actors/{id}", read.getId())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ActorReadDTO actualActor = objectMapper.readValue(resultJson, ActorReadDTO.class);
        Assert.assertEquals(read, actualActor);
    }

    @Test
    public void testDeleteActor() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/actors/{id}", id)).andExpect(status().isOk());

        Mockito.verify(actorService).deleteActor(id);
    }

    @Test
    public void testPutActor() throws Exception {
        ActorPutDTO put = new ActorPutDTO();
        testObjectsFactory.setFields(put);

        ActorReadDTO read = new ActorReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(actorService.upDateActor(read.getId(), put)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/actors/{id}", read.getId())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        ActorReadDTO actualActor = objectMapper.readValue(resultJson, ActorReadDTO.class);
        Assert.assertEquals(read, actualActor);
    }
}
