package homework.backend.example.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import homework.backend.example.domain.content.News;
import homework.backend.example.dto.news.NewsCreateDTO;
import homework.backend.example.dto.news.NewsPatchDTO;
import homework.backend.example.dto.news.NewsPutDTO;
import homework.backend.example.dto.news.NewsReadDTO;
import homework.backend.example.exception.EntityNotFoundException;
import homework.backend.example.service.NewsService;
import homework.backend.example.testfactory.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = NewsController.class)
public class NewsControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private NewsService newsService;

    private TestObjectsFactory testObjectsFactory = new TestObjectsFactory();

    @Test
    public void testGetNews() throws Exception {
        NewsReadDTO read = new NewsReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(newsService.getNews(read.getId())).thenReturn(read);

        String resultJson = mvc.perform(get("/api/v1/news/{id}", read.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        NewsReadDTO actualNews = objectMapper.readValue(resultJson, NewsReadDTO.class);
        Assertions.assertThat(read).isEqualToComparingFieldByField(actualNews);
    }

    @Test
    public void testDeleteNews() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/news/{id}", id)).andExpect(status().isOk());

        Mockito.verify(newsService).deleteNews(id);
    }

    @Test
    public void testPutNews() throws Exception {
        NewsPutDTO putDTO = new NewsPutDTO();
        testObjectsFactory.setFields(putDTO);

        NewsReadDTO read = new NewsReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(newsService.upDateNews(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/news/{id}", read.getId())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        NewsReadDTO actualNews = objectMapper.readValue(resultJson, NewsReadDTO.class);
        Assert.assertEquals(read, actualNews);
    }

    @Test
    public void testPatchNews() throws Exception {
        NewsPatchDTO patchDTO = new NewsPatchDTO();
        testObjectsFactory.setFields(patchDTO);

        NewsReadDTO read = new NewsReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(newsService.patchNews(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/news/{id}", read.getId())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        NewsReadDTO actualNews = objectMapper.readValue(resultJson, NewsReadDTO.class);
        Assert.assertEquals(read, actualNews);
    }

    @Test
    public void testGetNewsByWrongID() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(News.class, wrongId);
        Mockito.when(newsService.getNews(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/news/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testCreateNews() throws Exception {
        NewsCreateDTO create = new NewsCreateDTO();
        testObjectsFactory.setFields(create);

        NewsReadDTO read = new NewsReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(newsService.createNews(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/news")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        NewsReadDTO actualNews = objectMapper.readValue(resultJson, NewsReadDTO.class);
        Assertions.assertThat(actualNews).isEqualToComparingFieldByField(read);
    }
}
