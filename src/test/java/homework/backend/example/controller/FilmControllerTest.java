package homework.backend.example.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import homework.backend.example.domain.content.Film;
import homework.backend.example.dto.film.FilmCreateDTO;
import homework.backend.example.dto.film.FilmPatchDTO;
import homework.backend.example.dto.film.FilmPutDTO;
import homework.backend.example.dto.film.FilmReadDTO;
import homework.backend.example.exception.EntityNotFoundException;
import homework.backend.example.exception.hander.ErrorInfo;
import homework.backend.example.service.FilmService;
import homework.backend.example.testfactory.TestObjectsFactory;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = FilmController.class)
public class FilmControllerTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private FilmService filmService;

    private TestObjectsFactory testObjectsFactory = new TestObjectsFactory();

    @Test
    public void testGetFilm() throws Exception {
        FilmReadDTO read = new FilmReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(filmService.getFilm(read.getId())).thenReturn(read);

        String resultJson = mvc.perform(get("/api/v1/films/{id}", read.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        FilmReadDTO actualFilm = objectMapper.readValue(resultJson, FilmReadDTO.class);
        Assertions.assertThat(actualFilm).isEqualToComparingFieldByField(read);

        Mockito.verify(filmService).getFilm(read.getId());
    }

    @Test
    public void testGetFilmByWrongID() throws Exception {
        UUID wrongId = UUID.randomUUID();

        EntityNotFoundException exception = new EntityNotFoundException(Film.class, wrongId);
        Mockito.when(filmService.getFilm(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/films/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testNotCorrectIdFormat() throws Exception {
        String wrongID = "123";
        ErrorInfo exceptionError = new ErrorInfo(HttpStatus.BAD_REQUEST, MethodArgumentTypeMismatchException.class,
                "Failed to convert value of type 'java.lang.String' to required type 'java.util.UUID'; " +
                        "nested exception is java.lang.IllegalArgumentException: Invalid UUID string: " + wrongID);

        String resultJson = mvc.perform(get("/api/v1/films/{id}", wrongID))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        ErrorInfo actualError = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assertions.assertThat(actualError).isEqualToComparingFieldByField(exceptionError);
    }

    @Test
    public void testNotCorrectIdFormatSecondBranch() throws Exception {
        ErrorInfo exceptionError = new ErrorInfo(HttpStatus.BAD_REQUEST, HttpRequestMethodNotSupportedException.class,
                "Request method 'GET' not supported");

        String resultJson = mvc.perform(get("/api/v1/films"))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        ErrorInfo actualError = objectMapper.readValue(resultJson, ErrorInfo.class);
        Assertions.assertThat(actualError).isEqualToComparingFieldByField(exceptionError);
    }

    @Test
    public void testCreateFilm() throws Exception {
        FilmCreateDTO create = new FilmCreateDTO();
        testObjectsFactory.setFields(create);

        FilmReadDTO read = new FilmReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(filmService.createFilm(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/films")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        FilmReadDTO actualFilm = objectMapper.readValue(resultJson, FilmReadDTO.class);
        Assertions.assertThat(actualFilm).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchFilm() throws Exception {
        FilmPatchDTO patchDTO = new FilmPatchDTO();
        testObjectsFactory.setFields(patchDTO);

        FilmReadDTO read = new FilmReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(filmService.patchFilm(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/films/{id}", read.getId())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        FilmReadDTO actualFilm = objectMapper.readValue(resultJson, FilmReadDTO.class);
        Assert.assertEquals(read, actualFilm);
    }

    @Test
    public void testDeleteFilm() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/films/{id}", id)).andExpect(status().isOk());

        Mockito.verify(filmService).deleteFilm(id);
    }

    @Test
    public void testPutFilm() throws Exception {
        FilmPutDTO putDTO = new FilmPutDTO();
        testObjectsFactory.setFields(putDTO);

        FilmReadDTO read = new FilmReadDTO();
        testObjectsFactory.setFields(read);

        Mockito.when(filmService.upDateFilm(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/films/{id}", read.getId())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        System.out.println(resultJson);

        FilmReadDTO actualFilm = objectMapper.readValue(resultJson, FilmReadDTO.class);
        Assert.assertEquals(read, actualFilm);
    }
}
